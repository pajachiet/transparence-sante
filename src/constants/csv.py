# Separators
SEMICOLON_SEPARATOR = ';'
COMMA_SEPARATOR = ','
DEFAULT_SEPARATOR = SEMICOLON_SEPARATOR

# Source URL and local zip names
TRANSPARENCE_SANTE_URL = 'https://www.transparence.sante.gouv.fr/exports-etalab/exports-etalab.zip'
TRANSPARENCE_SANTE_ZIP_NAME = 'exports-etalab.zip'

# http://esante.gouv.fr/sites/default/files/asset/document/annuaire_sante_fr_dsft_fichier_extraction_rpps_donnees_accessibles_v1.0.0.pdf
HEALTH_PROFESSIONAL_URL = \
    "https://service.annuaire.sante.fr/annuaire-sante-webservices/V300/services/extraction/PS_LibreAcces"
HEALTH_PROFESSIONAL_ZIP_NAME = 'professionnels_sante.zip'

COMPANY_GROUP_URL = \
    "https://docs.google.com/spreadsheets/d/1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E/export?format=csv"

# CSV names
COMPANY_CSV = 'entreprise.csv'
REMUNERATION_CSV = 'declaration_remuneration.csv'
ADVANTAGE_CSV = 'declaration_avantage.csv'
CONVENTION_CSV = 'declaration_convention.csv'

COMPANY_GROUP_CSV = 'association_entreprise_groupe.csv'
COMPANY_GROUP_MISSING_COMPANIES_CSV = 'association_entreprise_groupe_avec_entreprise_manquantes.csv'

HEALTH_PROFESSIONAL_ACTIVITY_RAW_CSV = "PS_LibreAcces_Personne_activite.csv"
HEALTH_PROFESSIONAL_CSV = "professionnel_sante.csv"
HEALTH_ESTABLISHMENT_CSV = "etablissement_sante.csv"
HEALTH_PROFESSIONAL_ACTIVITY_CSV = "activite_professionnel_sante.csv"

# CSV lists
DECLARATION_CSV_LIST = [REMUNERATION_CSV,
                        ADVANTAGE_CSV,
                        CONVENTION_CSV]

ETALAB_CSV_LIST = [COMPANY_CSV] + DECLARATION_CSV_LIST

# REPERTOIRE SIRENE FROM OPEN DATA GOUV
SIRENE_URL = "http://files.data.gouv.fr/insee-sirene/StockUniteLegale_utf8.zip"
SIRENE_ZIP = "sirene.zip"
SIRENE_CSV = "StockUniteLegale"
NAF_URL = \
    "https://data.iledefrance.fr//explore/dataset/nomenclature-dactivites-francaise-naf-rev-2-code-ape/download" \
    "?format=csv&timezone=Europe/Berlin&use_labels_for_header=true"
NAF_CSV = 'naf.csv'
