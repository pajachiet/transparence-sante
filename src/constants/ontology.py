from anytree import Node, RenderTree

NO_INFORMATION = 'Non renseigné'

INTEREST = Node(
    parent=None,
    name="Conflict of interest",
    name_fr="Lien d'intérêt",
    name_efpia="Transfer of value",
    description_fr="Catégorie générale de lien d'intérêt financier",
    description_efpia=
    "Direct and indirect transfers of value, whether in cash, in kind or otherwise, made, whether for promotional "
    "purposes or otherwise, in connection with the development and sale of prescription-only Medicinal Products "
    "exclusively for human use. "
    "Direct transfers of value are those made directly by a Member Company for the benefit of a Recipient. "
    "Indirect transfers of value are those made on behalf of a Member Company for the benefit of a Recipient, "
    "or transfers of value made through an intermediate and where the Member Company knows or can identify the HCP/HCO "
    "that will benefit from the Transfer of Value."
)
DONATION_AND_GRANT = Node(
    parent=INTEREST,
    name="Donations and Grants",
    name_fr="Dons et Subventions",
    name_efpia="Donations and Grants to HCOs",
    description_efpia=
    "Donations and grants (in cash or in kind or otherwise) to institutions, organisations or associations "
    "that are comprised of healthcare professionals and/or that provide healthcare or conduct research "
    "(that are not otherwise covered by the EFPIA HCP Code or the EFPIA PO Code are only allowed if: "
    "(i) they are made for the purpose of supporting healthcare or research; "
    "(ii) they are documented and kept on record by the donor/grantor; and "
    "(iii) they do not constitute an inducement to recommend, prescribe, purchase, supply, sell or "
    "administer specific medicinal products. "
    "Donations and grants to individual healthcare professionals are not permitted under this section. "
    "Company sponsorship of healthcare professionals to attend international events is covered by Article 13. "
    "Companies are encouraged to make available publicly information about donations and grants "
    "(in cash or in kind or otherwise) made by them covered in this Section 11.01."
)
DONATION = Node(
    parent=DONATION_AND_GRANT,
    name="Donations",
    name_fr="Dons",
)
CASH_DONATION = Node(
    parent=DONATION,
    name="Cash donation",
    name_fr="Dons de sommes d'argent",
)
DONATION_IN_KIND = Node(
    parent=DONATION,
    name="Donation in kind",
    name_fr="Don en nature"
)
GRANT = Node(
    parent=DONATION_AND_GRANT,
    name="Grants",
    name_fr="Subventions",
)
EVENT = Node(
    parent=INTEREST,
    name="Contribution to costs of Events",
    name_fr="Contribution au coût d'événements promotionnels, scientifique ou professionnel",
    name_efpia="Contribution to costs of Events",
    description_efpia=
    "All promotional, scientific or professional meetings, congresses, conferences, symposia, and other similar events "
    "(including, but not limited to, advisory board meetings, visits to research or manufacturing facilities, "
    "and planning, training or investigator meetings for clinical trials and non-interventional studies) "
    "(each, an “Event”) organised or sponsored by or on behalf of a company. (Article 10 of the HCP Code)."
)
SPONSORSHIP = Node(
    parent=EVENT,
    name="Sponsorship",
    name_fr="Mécénat",
    name_efpia="Sponsorship agreements with HCOs / third parties appointed by HCOs to manage an Event",
)
REGISTRATION = Node(
    parent=EVENT,
    name="Registration Fees",
    name_fr="Frais d'inscriptions",
    name_efpia="Registration Fees",
)
TRAVEL_AND_HOSPITALITY = Node(
    parent=EVENT,
    name="Travel, Accommodation, Meals and Drinks",
    name_fr="Transport et Hospitalité",
    name_efpia="Travel & Accommodation",
)
TRAVEL = Node(
    parent=TRAVEL_AND_HOSPITALITY,
    name="Travel",
    name_fr="Transport",
)
HOSPITALITY = Node(
    parent=TRAVEL_AND_HOSPITALITY,
    name="Accommodation, Meals and Drinks",
    name_fr="Hospitalité",
)
ACCOMMODATION = Node(
    parent=HOSPITALITY,
    name="Accomodation",
    name_fr="Hébergement",
)
MEAL_AND_DRINK = Node(
    parent=HOSPITALITY,
    name="Meal and Drinks",
    name_fr="Restauration",
)
SERVICE_AND_CONSULTANCY = Node(
    parent=INTEREST,
    name="Service and Consultancy",
    name_fr="Service et Conseil",
    name_efpia="Fee for service and consultancy",
)
FEE = Node(
    parent=SERVICE_AND_CONSULTANCY,
    name="Fees",
    name_fr="Honoraires",
    name_efpia="Fees",
)
RELATED_EXPENSE = Node(
    parent=SERVICE_AND_CONSULTANCY,
    name="Related expenses",
    name_fr="Dépenses liées",
    name_efpia="Related expenses agreed in the fee for service or consultancy contract, "
               "including travel & accommodation relevant to the contract",
)
RESEARCH_AND_DEVELOPMENT = Node(
    parent=INTEREST,
    name="Research & Development",
    name_fr="Recherche et Développement",
    name_efpia="Research & Development",
)
MEAL_AND_DRINK_UNRELATED_TO_EVENT = Node(
    parent=INTEREST,
    name="Meals & Drinks unrelated to an event",
    name_fr="Repas et Boissons en dehors d'un événement",
)
GIFT = Node(
    parent=INTEREST,
    name="Gifts",
    name_fr="Cadeaux",
)
TRAINING = Node(
    parent=INTEREST,
    name="Training",
    name_fr="Formation",
)
UNCLASSIFIED = Node(
    parent=INTEREST,
    name="Unclassified",
    name_fr="Sans classe",
)
EMPTY_AND_OTHER = Node(
    parent=UNCLASSIFIED,
    name="Empty, Other",
    name_fr="Vide, Autre",
)
UNSUCESSFULL_MAPPING = Node(
    parent=UNCLASSIFIED,
    name="Unsucessfull mapping to a category",
    name_fr="Association à une catégorie non réussie",
)


def print_tree():
    for pre, fill, node in RenderTree(INTEREST):
        efpia_mark = '* ' if hasattr(node, 'name_efpia') else ''
        # print("{}{}{} | {}".format(pre, efpia_mark, node.name, node.name_fr))
        print("{}{}{}".format(pre, efpia_mark, node.name_fr))


if __name__ == '__main__':
    print_tree()