import logging
import sys
from time import sleep
from typing import List, Union, Set

import pandas as pd
import requests

from settings import METABASE_USERNAME, METABASE_PASSWORD, METABASE_BASE_URL

ENTERPRISE = "entreprise"

ENTERPRISE_FIELD_ID = 134
ENTERPRISE_CARD_ID = 154
ENTERPRISE_PUBLIC_DASHBOARD_ID = 227

MAIN_DASHBOARDS = [2, 8, 72]
MAIN_PUBLIC_DASHBOARDS = [ENTERPRISE_PUBLIC_DASHBOARD_ID, 228, 229]


def metabase_get_token() -> str:
    payload = {"username": METABASE_USERNAME, "password": METABASE_PASSWORD}
    r = requests.post(METABASE_BASE_URL + "/api/session", json=payload)
    if not str(r.status_code).startswith('2'):
        raise ValueError("We could not get token. Endpoint returned with status code '{}', with message : '{}'"
                         .format(r.status_code, r.text[:300]))
    return r.json()['id']


def metabase_get(endpoint: str, token: str, params: dict = None) -> Union[dict, Set[dict]]:
    if params is None:
        params = {}
    r = requests.get(METABASE_BASE_URL + endpoint, headers={"X-Metabase-Session": token}, params=params)
    if not str(r.status_code).startswith('2'):
        raise ValueError("Get requests on endpoint '{}' returned with non 2xx status code '{}' and message '{}'"
                         .format(endpoint, r.status_code, r.text[:300]))
    return r.json()


def metabase_post(endpoint: str, token: str, payload: dict = None) -> dict:
    if payload is None:
        payload = {}
    r = requests.post(METABASE_BASE_URL + endpoint, headers={"X-Metabase-Session": token}, json=payload)
    if not str(r.status_code).startswith('2'):
        raise ValueError("Post requests on endpoint '{}' returned with non 2xx status code '{}' and message '{}'"
                         .format(endpoint, r.status_code, r.text[:300]))
    return r.json()


def metabase_query_card(card_id: int, token: str, ignore_cache: bool = True, parameters: list = None) -> dict:
    endpoint = "/api/card/{}/query".format(card_id)
    payload = {
        "ignore_cache": ignore_cache,
        "parameters": parameters or []
    }
    return metabase_post(endpoint, token, payload)


def metabase_query_cards(cards_id: Set[int], token: str, ignore_cache: bool = True, parameters: list = None) -> None:
    for card_id in cards_id:
        logging.debug("Query card {}".format(card_id))
        result = metabase_query_card(card_id, token, ignore_cache, parameters)
        if result['status'] == 'completed':
            logging.info("Question completed in {time:>4}ms - {id:>3}"
                         .format(id=card_id, time=result['running_time']))
        else:
            logging.warning("Question failed - {id:>3} - {url}/question/{id}"
                            .format(id=card_id, url=METABASE_BASE_URL))


def find_dashboards_containing_card(card_id: int, token: str) -> List[int]:
    dashboards_containing_card = list()
    shallow_dashboard_list = metabase_get("/api/dashboard/", token)
    for shallow_dashboard in shallow_dashboard_list:
        dashboard_id = shallow_dashboard['id']
        dashboard = metabase_get("/api/dashboard/{}".format(dashboard_id), token)
        for ordered_card in dashboard['ordered_cards']:
            if ordered_card['card_id'] == card_id:
                logging.info("Card {card_id} is present in dashboard {id:>3} : {name}"
                             .format(card_id=card_id, id=dashboard_id, name=dashboard['name']))
                dashboards_containing_card.append(dashboard_id)

    if not dashboards_containing_card:
        logging.info("Card {} was not found in any active dashboard".format(card_id))
    return dashboards_containing_card


def get_dashboards_in_root_collection(token) -> List[int]:
    dashboard_list = metabase_get("/api/dashboard/", token)
    for dashboard in dashboard_list:
        if dashboard['collection_id'] is None:
            yield dashboard['id']


def get_cards_in_dashboard_list(dashboard_id_list: List[int], token) -> Set[int]:
    cards = dict()
    for dashboard_id in dashboard_id_list:
        dashboard = metabase_get("/api/dashboard/{}".format(dashboard_id), token)
        for card_container in dashboard['ordered_cards']:
            card = card_container['card']
            if 'id' in card:
                cards[card['id']] = card

    return {c['id'] for c in cards.values()}


def query_main_cards():
    token = metabase_get_token()
    # dashboards_in_root_collection = get_dashboards_in_root_collection(token)
    cards_id = get_cards_in_dashboard_list(MAIN_DASHBOARDS + MAIN_PUBLIC_DASHBOARDS, token)
    logging.info("Query cards from main dashboards and their public version")
    metabase_query_cards(cards_id, token, ignore_cache=False)

    query_enterprise_public_dashboard_with_filter(token)


# def connect_metabase_query_all_cards():
#     token = metabase_get_token()
#     cards = metabase_get("/api/card/", token)
#     cards_id = {c['id'] for c in cards}
#     metabase_query_cards(cards_id, token)


def wait_for_metabase(max_time=300):
    logging.info("Waiting until Metabase accept connexions")
    for i in range(max_time):
        try:
            r = requests.get(METABASE_BASE_URL + "/api/health")
            if str(r.status_code).startswith('2'):
                sys.stdout.write('\n')
                sys.stdout.flush()
                return True
        except (requests.exceptions.ConnectionError, ValueError):
            pass
        sys.stdout.write('.')
        sys.stdout.flush()
        sleep(1)

    logging.info("Exceeding max waiting time of {}s".format(max_time))
    return False


def query_enterprise_public_dashboard_with_filter(token):
    cards_id = get_cards_in_dashboard_list([ENTERPRISE_PUBLIC_DASHBOARD_ID], token)
    df_enterprise = get_df_enterprise(token)

    head_100_companies = df_enterprise[ENTERPRISE].head(100)
    logging.info(f"Query cards from public Enterprise dashboard with the first 100 companies : " +
                 ' ,'.join(head_100_companies))

    for i, enterprise_name in enumerate(head_100_companies):
        logging.debug(f"Query public Enterprise dashboard for n-th enterprise {i} '{enterprise_name}'")
        parameters = [get_enterprise_filter(enterprise_name)]
        metabase_query_cards(cards_id, token, ignore_cache=False, parameters=parameters)


def get_df_enterprise(token) -> pd.DataFrame:
    """Entreprises par nombre et montant"""
    result = metabase_query_card(ENTERPRISE_CARD_ID, token, ignore_cache=False)
    df = pd.DataFrame(result['data']['rows'], columns=[ENTERPRISE, "nombre", "montant"])
    return df.sort_values("nombre", ascending=False)


def get_enterprise_filter(enterprise_name: str) -> dict:
    return {
        "target": ["dimension", ["field-id", ENTERPRISE_FIELD_ID]],
        "type": "category",
        "value": [enterprise_name]
    }


if __name__ == '__main__':
    wait_for_metabase()

    token = metabase_get_token()
    metabase_query_cards([10, 75], token, ignore_cache=False)
    query_main_cards()
    find_dashboards_containing_card(7, metabase_get_token())
