import logging
from time import sleep

from sqlalchemy import create_engine, text
from sqlalchemy.engine import Engine
from sqlalchemy.exc import OperationalError

from settings import POSTGRES_HOST, POSTGRES_PORT, POSTGRES_PASSWORD
from src.constants.connexions import POSTGRES_USER
from src.utils.utils import timeit

POSTGRES_URL = "postgresql://{user}:{password}@{host}:{port}".format(user=POSTGRES_USER,
                                                                     password=POSTGRES_PASSWORD,
                                                                     host=POSTGRES_HOST,
                                                                     port=POSTGRES_PORT)


def create_postgres_engine():
    # Without AUTOCOMMIT's isolation_level, "CREATE MATERIALIZED VIEW" query silently fails
    # when there is a first line "SET work_mem … ;"
    return create_engine(POSTGRES_URL, isolation_level='AUTOCOMMIT')


@timeit
def execute_text_query(engine, raw_query, verbose=True):
    query = text(raw_query)
    if verbose:
        logging.debug("Execute query : {}".format(query))
    result = engine.execute(query)
    if result.returns_rows:
        for row in result:
            logging.info(row)


def wait_for_postgres(max_waiting_time: int = 30) -> None:
    engine = create_postgres_engine()
    logging.info('Waiting until PostgreSQL accept connexions')
    for i in range(max_waiting_time):
        if does_postgres_accept_connection(engine):
            logging.info('PostgreSQL is ready to accept connexions')
            return
        logging.info('PostgreSQL is not ready to accept connexions, waiting {} more seconds'
                     .format(max_waiting_time - i))
        sleep(1)

    engine.connect()  # Raise exception


def create_trgm_extension() -> None:
    engine = create_postgres_engine()
    execute_text_query(engine, "CREATE EXTENSION IF NOT EXISTS pg_trgm;")


def does_postgres_accept_connection(engine: Engine) -> bool:
    try:
        engine.connect()
    except OperationalError:
        return False
    else:
        return True
