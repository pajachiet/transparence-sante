import logging
import os
import time
from collections import OrderedDict


# Column names
def quoted(column_name):
    return '"{}"'.format(column_name)


def comma_separated_columns(columns):
    return ', '.join([quoted(column_name) for column_name in columns])


def default_columns(columns):
    return ','.join([quoted(column_name) for column_name in columns])


# Files
def file_exists(file_path):
    if os.path.exists(file_path):
        logging.warning("File '{}' already exists. Delete it before if you want to update it.".format(file_path))
        return True
    return False


def remove_file(file_path):
    if os.path.exists(file_path):
        logging.info("Removing file at '{}'".format(file_path))
        os.remove(file_path)
        logging.debug("- file '{}' removed".format(file_path))


def prefix(file_name):
    return os.path.splitext(file_name)[0]


SUFFIXES = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']


def human_size(file_path):
    nbytes = os.path.getsize(file_path)
    i = 0
    while nbytes >= 1024 and i < len(SUFFIXES) - 1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, SUFFIXES[i])


# Technical
def merge_dict(dictionary_list):
    merged_dict = OrderedDict()
    for dictionary in dictionary_list:
        merged_dict.update(dictionary)
        merged_dict.update(dictionary)
    return merged_dict


def timeit(function_to_time):
    def timed(*args, **kwargs):
        start_time = time.time()
        result = function_to_time(*args, **kwargs)
        end_time = time.time()

        logging.info("Function '{}' took {:.2f} seconds"
                     .format(function_to_time.__name__, end_time - start_time))
        return result

    return timed


def is_running_in_docker():
    proc_cgroup_file = os.path.join('/', 'proc', 'self', 'cgroup')
    if not os.path.exists(proc_cgroup_file):
        return False

    with open(proc_cgroup_file, 'r') as f:
        for line in f.readlines():
            t = line.split('/')
            if len(t) < 2:
                continue
            if t[1] == 'docker':
                return True

    return False


# Developer interaction
def get_confirmation(question, positive_answer="I confirm", negative_answer="No"):
    valid = {positive_answer.lower(): True, negative_answer.lower(): False}
    answer_information = "Please answer with '{}' or '{}'.".format(positive_answer, negative_answer)
    print(question)
    while True:
        print(answer_information)
        answer = input()
        if answer.lower() in valid:
            if valid[answer.lower()]:
                return
            else:
                print("Aborting")
                exit(0)
        else:
            print("Your answer was '{}'".format(answer))


# Test utils
def add_final_directory_to_file_path(file_path, intermediate_directory):
    head, tail = os.path.split(file_path)
    return os.path.join(head, intermediate_directory, tail)


# Settings utils
def env_flag(env_var, default=False):
    """
    Return the specified environment variable coerced to a bool, as follows:
    - When the variable is unset, or set to the empty string, return `default`.
    - When the variable is set to a truthy value, returns `True`.
      These are the truthy values:
          - 1
          - true, yes, on
    - When the variable is set to the anything else, returns False.
       Example falsy values:
          - 0
          - no
    - Ignore case and leading/trailing whitespace.
    From https://github.com/bodylabs/env-flag
    """
    import os
    environ_string = os.environ.get(env_var, '').strip().lower()
    if not environ_string:
        return default
    return environ_string in ['1', 'true', 'yes', 'on']
