""" Helper function for tests
"""
import os

import pandas as pd

from src.constants import csv

CSV_1 = "1.csv"
CSV_2 = "2.csv"
COMPARE_CSV_FIXTURE_DIR = os.path.join("tests", "fixture", "compare_csv")
DIFFERENT_COLUMNS_SETS = "different_columns_sets"
DIFFERENT_COLUMNS_ORDER = "different_columns_order"
DIFFERENT_LINE_NUMBER = "different_line_number"
DIFFERENT_CONTENT = "different_content"
DIFFERENCE = "difference"


def assert_csv_files_are_equals(csv_path_1, csv_path_2):
    assert_csv_have_same_columns(csv_path_1, csv_path_2)
    assert_csv_have_same_content(csv_path_1, csv_path_2)


def assert_csv_have_same_columns(csv_path_1, csv_path_2, sep=csv.DEFAULT_SEPARATOR):
    columns_1 = pd.read_csv(csv_path_1, sep=sep).columns
    columns_2 = pd.read_csv(csv_path_2, sep=sep).columns

    # Same set
    additional_columns_1 = set(columns_1) - set(columns_2)
    additional_columns_2 = set(columns_2) - set(columns_1)
    assert not additional_columns_1, "First csv has additional columns {}.".format(additional_columns_1)
    assert not additional_columns_2, "Second csv has additional columns {}.".format(additional_columns_2)

    # Same order
    for name_1, name_2 in zip(columns_1, columns_2):
        assert name_1 == name_2, "CSV have different columns order : '{}' != '{}'".format(name_1, name_2)


def assert_csv_have_same_content(csv_path_1, csv_path_2, sep=csv.DEFAULT_SEPARATOR):
    df1 = pd.read_csv(csv_path_1, sep=sep)
    df2 = pd.read_csv(csv_path_2, sep=sep)

    assert len(df1) == len(df2), "CSV have different line number, respectively {} and {}".format(len(df1), len(df2))

    assert df1.equals(df2), "CSV have different content at the following position (rows counting from 0) :\n{}".format(
        get_difference_position(df1, df2))


def get_difference_position(df1, df2):
    na_filler = "NA_filler_468435"
    return ((df1.fillna(na_filler) != df2.fillna(na_filler))
            .replace(False, pd.np.nan)
            .dropna(axis='index', how='all')
            .dropna(axis='columns', how='all')
            .replace(1, DIFFERENCE)
            .fillna("")
            )


def get_fixture_csv_pathes(directory_name):
    return os.path.join(COMPARE_CSV_FIXTURE_DIR, directory_name, CSV_1), \
           os.path.join(COMPARE_CSV_FIXTURE_DIR, directory_name, CSV_2)


if __name__ == '__main__':
    csv_1, csv_2 = get_fixture_csv_pathes(DIFFERENT_CONTENT)
    assert_csv_files_are_equals(csv_1, csv_2)
