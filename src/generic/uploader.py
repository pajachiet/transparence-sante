import logging
import os
from typing import Dict

from src.constants.csv import SEMICOLON_SEPARATOR
from src.constants.directories import ROOTED_CLEANED_DATA_DIR
from src.generic.ddl.table import TableDDL
from src.utils.dataframe_io import get_csv_columns, check_file_exists
from src.utils.postgres import create_postgres_engine
from src.utils.utils import timeit, comma_separated_columns


class Uploader:
    SEPARATOR = SEMICOLON_SEPARATOR

    COLUMN_LINE_TEMPLATE = "    {quoted_column_name} {column_type}"
    VACUUM_ANALYSE_TEMPLATE = "VACUUM ANALYSE {};"

    def __init__(self, csv_name,
                 csv_directory=ROOTED_CLEANED_DATA_DIR,
                 table_name=None,
                 columns_with_non_default_type: Dict[str, str] = None
                 ):
        self.csv_directory = csv_directory
        self.csv_name = csv_name
        self.csv_path = os.path.join(csv_directory, csv_name)
        self.table_name = table_name or csv_name[:-4]
        self.columns_with_non_default_type = columns_with_non_default_type or dict()
        self.engine = create_postgres_engine()

    @property
    def columns(self):
        return get_csv_columns(self.csv_path, separator=self.SEPARATOR)

    def reset(self):
        self.table_ddl.reset()

    def run(self):
        self.table_ddl.run()
        self._copy_csv_to_table()

    @timeit
    def _copy_csv_to_table(self):
        logging.info("Load csv '{}' into table '{}'".format(self.csv_path, self.table_name))
        check_file_exists(self.csv_path)
        connection = self.engine.raw_connection()
        with connection.cursor() as cursor:
            cursor.execute("SET datestyle = 'ISO,DMY';")
            with open(self.csv_path, 'rb') as csv_file:
                csv_file.readline()
                cursor.copy_expert(
                    """COPY {table_name}({columns}) FROM STDIN
                    WITH (FORMAT CSV, DELIMITER '{delimiter}', ENCODING utf8)"""
                        .format(table_name=self.table_name,
                                columns=comma_separated_columns(self.columns),
                                delimiter=self.SEPARATOR),
                    csv_file
                )
        connection.commit()
        self.table_ddl.vacuum_analyse()

    @property
    def table_ddl(self):
        return TableDDL(self.table_name, self.columns, self.columns_with_non_default_type)
