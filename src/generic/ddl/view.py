from src.generic.ddl.abstract import DDL
from src.generic.ddl.postgres_entities import VIEW


class View(DDL):
    DDL_ENTITY = VIEW

    def __init__(self, name, ddl_template, **template_kwargs):
        super().__init__()
        self.name = name
        self.ddl_template = ddl_template
        self.template_kwargs = template_kwargs

    @property
    def query(self):
        return self.ddl_template.format(name=self.name, **self.template_kwargs)