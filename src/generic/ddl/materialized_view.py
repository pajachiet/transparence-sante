from settings import POSTGRES_WORK_MEM
from src.generic.ddl.abstract import VacuumedDDL
from src.generic.ddl.index import SearchIndexDDL, IndexDDL
from src.generic.ddl.postgres_entities import MATERIALIZED_VIEW
from src.utils.utils import comma_separated_columns


class MaterializedViewDDL(VacuumedDDL):
    DDL_ENTITY = MATERIALIZED_VIEW

    DDL_TEMPLATE = """
SET work_mem = '{work_mem}'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW "{name}" AS ({select_query}
);
RESET work_mem;
"""

    def __init__(self, name, select_query, search_indices):
        super().__init__()
        self.name = name
        self.select_query = select_query
        self.search_indices = search_indices

    @property
    def query(self):
        ddl = self.DDL_TEMPLATE.format(work_mem=POSTGRES_WORK_MEM, **self.__dict__)
        for search_index in self.search_indices:
            ddl += SearchIndexDDL(self.name, search_index).query
        return ddl


class AggregatedMaterializedViewDDL(MaterializedViewDDL):
    AGGREGATED_VALUE_TEMPLATE = "{aggregation}_{value}"
    NAME_TEMPLATE = "{table}_{aggregation}_{value}_by_{group_columns_underscore}"

    SELECT_QUERY_TEMPLATE = """
    SELECT
        {group_column_comma},
        {aggregation}("{value}") AS "{aggregated_value}"
    FROM "{table}"
    WHERE (
        {group_column_not_null}
        AND ("{value}" IS NOT NULL)
    )
    GROUP BY {group_column_comma}
    ORDER BY ({aggregation}("{value}")) DESC"""

    def __init__(self, table, value, group_columns, aggregation="sum"):
        self.table = table
        self.aggregation = aggregation
        self.value = value
        self.group_column = group_columns if type(group_columns) == list else [group_columns]
        self.aggregated_value = self.AGGREGATED_VALUE_TEMPLATE.format(**self.__dict__)
        super().__init__(
            name=self.NAME_TEMPLATE.format(
                group_columns_underscore='_'.join(self.group_column),
                **self.__dict__
            ),
            select_query=self.SELECT_QUERY_TEMPLATE.format(
                group_column_comma=comma_separated_columns(self.group_column),
                group_column_not_null=' AND '.join(['("{}" IS NOT NULL)'.format(group_column)
                                                    for group_column in self.group_column
                                                    ]),
                **self.__dict__),
            search_indices=[self.group_column]
        )

    @property
    def query(self):
        ddl = self.DDL_TEMPLATE.format(work_mem=POSTGRES_WORK_MEM, **self.__dict__)
        for group_column in self.group_column:
            ddl += SearchIndexDDL(self.name, group_column).query
        ddl += IndexDDL(self.name, self.aggregated_value).query
        return ddl
