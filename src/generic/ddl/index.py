from src.generic.ddl.abstract import DDL
from src.generic.ddl.postgres_entities import INDEX


class IndexDDL(DDL):
    DDL_ENTITY = INDEX
    NAME_TEMPLATE = "{table}_index_on_{index_column}"
    DDL_TEMPLATE = """
CREATE {unique} INDEX IF NOT EXISTS "{name}"
ON public."{table}" ("{index_column}");
"""

    def __init__(self, table, index_column, unique=False):
        super(IndexDDL, self).__init__()
        self.table = table
        self.index_column = index_column
        self.unique = 'UNIQUE' if unique else ''
        self.name = self.NAME_TEMPLATE.format(**self.__dict__)

    @property
    def query(self):
        return self.DDL_TEMPLATE.format(**self.__dict__)


class SearchIndexDDL(DDL):
    DDL_ENTITY = INDEX
    NAME_TEMPLATE = "{table}_trgm_gin_index_on_{index_column}"
    DDL_TEMPLATE = """
CREATE INDEX IF NOT EXISTS "{name}"
ON public."{table}" USING GIN (lower("{index_column}") gin_trgm_ops);
"""

    def __init__(self, table, index_column, unique=False):
        super(SearchIndexDDL, self).__init__()
        self.table = table
        self.index_column = index_column
        self.unique = unique
        self.name = self.NAME_TEMPLATE.format(**self.__dict__)

    @property
    def query(self):
        ddl = self.DDL_TEMPLATE.format(**self.__dict__)
        ddl += IndexDDL(self.table, self.index_column, self.unique).query
        return ddl
