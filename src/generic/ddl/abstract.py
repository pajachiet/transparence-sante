import logging
import os
from abc import ABC, abstractmethod

from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from src.constants.directories import DDL_DIR
from src.generic.ddl.postgres_entities import INDEX, FOREIGN_KEY
from src.utils.postgres import create_postgres_engine, execute_text_query


class DDL(ABC):
    STR_TEMPLATE = "CREATE_{entity}_{name}"
    DDL_ENTITY = ""
    DROP_TEMPLATE = "DROP {entity} IF EXISTS {name} CASCADE;\n"

    def __init__(self):
        self.engine = create_postgres_engine()

    def reset(self):
        logging.info("Reset {}".format(self.name))
        drop_ddl = self.DROP_TEMPLATE.format(entity=self.DDL_ENTITY, **self.__dict__)
        execute_text_query(self.engine, drop_ddl)

    def run(self):
        self.write_ddl()
        logging.info("Run query of DDL '{}'".format(self.name))
        execute_text_query(self.engine, self.query)

    def write_ddl(self):
        if self.DDL_ENTITY not in [INDEX, FOREIGN_KEY]:
            logging.info("Writing DDL to '{}'".format(self.path))
            with open(self.path, 'wb') as ddl_file:
                ddl_file.write(self.query.encode("utf-8"))
        else:
            logging.debug("Skipping writing DDL for index")

    @property
    def path(self):
        return os.path.join(DDL_DIR, self.name + '.sql')

    @property
    @abstractmethod
    def query(self):
        pass


class VacuumedDDL(DDL):
    VACUUM_ANALYSE_TEMPLATE = """VACUUM ANALYSE "{name}";"""

    def __init__(self):
        super(VacuumedDDL, self).__init__()

    def run(self):
        super().run()
        self.vacuum_analyse()

    def vacuum_analyse(self):
        connection = self.engine.raw_connection()
        connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        vacuum_analyse_query = self.VACUUM_ANALYSE_TEMPLATE.format(name=self.name)
        with connection.cursor() as cursor:
            logging.debug("Execute query : {}".format(vacuum_analyse_query))
            cursor.execute(vacuum_analyse_query)
