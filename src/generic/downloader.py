import logging
import os
import zipfile

import requests
import urllib3

from settings import CSV_CHUNK_SIZE, ROWS_LIMIT
from src.constants.directories import ROOTED_RAW_DATA_DIR
from src.utils.utils import timeit, prefix, file_exists, remove_file


class Downloader:
    def __init__(self, url, file_name, verify_ssl=True):
        self.url = url
        self.directory = ROOTED_RAW_DATA_DIR
        self.file_name = file_name
        self.file_path = os.path.join(self.directory, self.file_name)
        self.verify_ssl = verify_ssl

    def reset(self):
        remove_file(self.file_path)

    @timeit
    def run(self):
        if not file_exists(self.file_path):
            self.download_file_from_url()

    def download_file_from_url(self):
        logging.info("Download file at url {} to '{}'".format(self.url, self.file_path))
        if not self.verify_ssl:
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        r = requests.get(self.url, stream=True, verify=self.verify_ssl)
        with open(self.file_path, 'wb') as f:
            for chunk in r.iter_content(CSV_CHUNK_SIZE):
                f.write(chunk)


class Extractor:
    def __init__(self, zip_archive_name, file_name):
        self.directory = ROOTED_RAW_DATA_DIR
        self.zip_archive_name = zip_archive_name
        self.zip_path = os.path.join(self.directory, self.zip_archive_name)

        self.file_name = file_name
        self.file_path = os.path.join(self.directory, self.file_name)

    @property
    def zf(self):
        return zipfile.ZipFile(self.zip_path)

    def reset(self):
        remove_file(self.file_path)

    @timeit
    def run(self):
        logging.debug("Extracting file {} from {}".format(self.file_name, self.zip_path))
        if not file_exists(self.file_path):
            self.extract_file()

    def extract_file(self):
        zip_file_name = self.get_zip_file_matching_prefix()
        logging.info("Extracting '{}' from '{}' to '{}'".format(zip_file_name, self.zip_path, self.file_path))

        with self.zf.open(zip_file_name) as input_file, open(self.file_path, 'wb') as output_file:
            for i, line in enumerate(input_file):
                if i > ROWS_LIMIT:
                    return
                output_file.write(line)

    def get_zip_file_matching_prefix(self):
        file_prefix = prefix(self.file_name)
        for zip_file_name in self.zf.namelist():
            if zip_file_name.startswith(file_prefix):
                return zip_file_name
        raise FileNotFoundError("File with prefix '{}' was not found in zip archive.".format(file_prefix))
