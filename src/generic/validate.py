class ValidationError(Exception):
    def __init__(self, message):
        super().__init__(message)


def get_checker_columns_set(columns_set):
    def check_dataframe_has_correct_columns_set(df):
        df_columns_set = set(df.columns)
        if columns_set != df_columns_set:
            raise ValidationError("Dataframe misses columns '{}', and has additional columns '{}' ".format(
                df_columns_set - columns_set,
                columns_set - df_columns_set
            ))

    return check_dataframe_has_correct_columns_set


def get_checker_columns_have_no_null(columns_without_null):
    def check_columns_have_no_null(df):
        for column_name in columns_without_null:
            if df[column_name].isna().any():
                raise ValidationError("Column '{}' should not contain any NULL value".format(column_name))

    return check_columns_have_no_null


def get_checker_columns_are_unique(unique_columns):
    def check_that_columns_are_unique(df):
        for column_name in unique_columns:
            if not df[column_name].is_unique:
                raise ValidationError("'{}' is not unique.".format(column_name))

    return check_that_columns_are_unique
