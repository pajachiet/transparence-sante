import logging

from src.constants import column, tables
from src.constants import csv
from src.generic.cleaner import Cleaner
from src.generic.ddl.foreign_key import ForeignKeyDDL
from src.generic.ddl.index import IndexDDL
from src.generic.uploader import Uploader
from src.specific.health_directoy import deduplication_functions


def get_full_na_rows_dropper(id_column):
    def drop_full_na_rows(df):
        non_id_columns = list(df.columns)
        non_id_columns.remove(id_column)
        full_na_rows = df[non_id_columns].isna().all(axis=1)
        logging.info("Dropping {} fully null rows".format(full_na_rows.sum()))
        return df[~ full_na_rows]

    return drop_full_na_rows


cleaner = Cleaner(
    input_csv_name=csv.HEALTH_PROFESSIONAL_ACTIVITY_RAW_CSV,
    output_csv_name=csv.HEALTH_PROFESSIONAL_ACTIVITY_CSV,
    input_separator='|',
    chunk_size=10 ** 7,  # We need to clean the whole data set in one go in order to remove duplicates
    keep_and_rename_columns={
        # IDENTIFICATION
        'Identifiant PP': column.PROFESSIONAL_ID,

        # ACTIVITY
        'Libellé mode exercice': column.EMPLOYMENT_STATUS,

        # STRUCTURE
        'Identifiant technique de la structure': column.ORGANIZATION_ID,

    },
    piped_transformations=[
        get_full_na_rows_dropper(column.PROFESSIONAL_ID),
        deduplication_functions.drop_duplicates,
    ],
    columns_not_null=[
        column.PROFESSIONAL_ID,
    ],
)

uploader = Uploader(csv.HEALTH_PROFESSIONAL_ACTIVITY_CSV,
                    table_name=tables.HEALTH_PROFESSIONAL_ACTIVITY)

INDEX_COLUMNS = [
    column.PROFESSIONAL_ID,
    column.ORGANIZATION_ID,
    column.EMPLOYMENT_STATUS,
]
indices = [
    IndexDDL(tables.HEALTH_PROFESSIONAL_ACTIVITY, column_name)
    for column_name in INDEX_COLUMNS
]

foreign_keys = [
    ForeignKeyDDL(tables.HEALTH_PROFESSIONAL_ACTIVITY,
                  column.PROFESSIONAL_ID,
                  tables.HEALTH_PROFESSIONAL_DIRECTORY,
                  ),
    ForeignKeyDDL(tables.HEALTH_PROFESSIONAL_ACTIVITY,
                  column.ORGANIZATION_ID,
                  tables.HEALTH_ESTABLISHMENT_DIRECTORY,
                  ),
]

if __name__ == '__main__':
    cleaner.reset()
    cleaner.run()