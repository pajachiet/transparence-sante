import logging

from src.constants import column
from src.constants.column import FULL_PREFIX_SEARCH, MINIMAL_PREFIX_SEARCH
from src.specific.utils import concat_str_columns_from_df_with_separator, concat_two_str_series


def create_full_prefix_search_string(df):
    logging.info("Create a full unique prefix search string, for each professional, starting from '{}'"
                 .format(column.NAME_SURNAME))

    df[FULL_PREFIX_SEARCH] = concat_str_columns_from_df_with_separator(df, column.NAME_SURNAME, [
        (column.PROFESSION, ', '),
        (column.SPECIALTY, ', '),
        (column.TOWN, ', à '),
        (column.ID_TYPE, ', n°'),
        (column.PROFESSIONAL_ID, ' '),
    ])
    return df


def create_minimal_prefix_search_string(df):
    logging.info("Create a minimal unique prefix search string, for each professional, starting from '{}'"
                 .format(column.NAME_SURNAME))

    df[MINIMAL_PREFIX_SEARCH] = df[column.NAME_SURNAME]
    concat_other_column_to_prefix_search(df, MINIMAL_PREFIX_SEARCH, column.PROFESSION)
    concat_other_column_to_prefix_search(df, MINIMAL_PREFIX_SEARCH, column.SPECIALTY)
    concat_other_column_to_prefix_search(df, MINIMAL_PREFIX_SEARCH, column.TOWN, sep=', à ')
    concat_other_column_to_prefix_search(df, MINIMAL_PREFIX_SEARCH, column.PROFESSIONAL_ID, sep=', n°',
                                         column_to_select_duplicates=column.NAME_SURNAME)
    return df


def concat_other_column_to_prefix_search(df, column_to_extend, column_to_add, sep=', ',
                                         column_to_select_duplicates=None):
    column_to_select_duplicates = column_to_select_duplicates or column_to_extend

    mask_duplicated = df[column_to_select_duplicates].duplicated(keep=False)
    logging.info("Concat column '{}' to {} rows to deduplicate column '{}'"
                 .format(column_to_add, mask_duplicated.sum(), column_to_select_duplicates))
    df_duplicated = df.loc[mask_duplicated]
    s_new = concat_two_str_series(df_duplicated[column_to_extend], df_duplicated[column_to_add], sep=sep)
    df[column_to_extend].update(s_new)
