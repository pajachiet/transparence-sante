from src.constants import column, tables
from src.constants import csv
from src.generic.cleaner import Cleaner
from src.generic.ddl.index import IndexDDL, SearchIndexDDL
from src.generic.downloader import Downloader, Extractor
from src.generic.uploader import Uploader
from src.specific.declaration.functions import concat_name_surname
from src.specific.health_directoy import deduplication_functions
from src.specific.health_directoy.search_string import create_full_prefix_search_string, \
    create_minimal_prefix_search_string
from src.specific.normalize_functions import normalize_name, normalize_surname, normalize_specialty
from src.specific.types import index_list

downloader = Downloader(csv.HEALTH_PROFESSIONAL_URL, csv.HEALTH_PROFESSIONAL_ZIP_NAME, verify_ssl=False)
extractor = Extractor(csv.HEALTH_PROFESSIONAL_ZIP_NAME, csv.HEALTH_PROFESSIONAL_ACTIVITY_RAW_CSV)


def replace_miss_by_madame(df):
    # http://circulaire.legifrance.gouv.fr/pdf/2012/02/cir_34682.pdf
    return df[column.TITLE].str.replace('Mademoiselle', 'Madame')


cleaner = Cleaner(
    input_csv_name=csv.HEALTH_PROFESSIONAL_ACTIVITY_RAW_CSV,
    output_csv_name=csv.HEALTH_PROFESSIONAL_CSV,
    input_separator='|',
    chunk_size=10 ** 7,  # We need to clean the whole data set in one go in order to remove duplicates
    keep_and_rename_columns={
        # 'Identification nationale PP': 'Identification nationale PP',
        "Type d'identifiant PP": column.ID_TYPE,
        'Identifiant PP': column.PROFESSIONAL_ID,
        'Libellé civilité': column.TITLE,
        "Nom d'exercice": column.NAME,
        "Prénom d'exercice": column.SURNAME,
        'Libellé catégorie professionnelle': column.PROFESSIONAL_CATEGORY,
        "Libellé civilité d'exercice": column.PROFESSIONAL_TITLE,
        'Libellé profession': column.PROFESSION,
        'Libellé savoir-faire': column.SPECIALTY,
        'Libellé commune (coord. structure)': column.TOWN,  # Choose unique one
        # 'Libellé type savoir-faire': 'Libellé type savoir-faire',
    },
    assign_columns_with_functions={
        column.TITLE: replace_miss_by_madame,
        column.NAME: normalize_name,
        column.SURNAME: normalize_surname,
        column.NAME_SURNAME: concat_name_surname,
        column.SPECIALTY: normalize_specialty,
        column.ID_TYPE: lambda df: df[column.ID_TYPE].map({'0': "ADELI", '8': "RPPS"}),
    },
    piped_transformations=[
        deduplication_functions.drop_multiple_professional_category,
        deduplication_functions.drop_multiple_profession,
        deduplication_functions.drop_multiple_specialty,
        deduplication_functions.drop_multiple_town,
        deduplication_functions.drop_duplicates,
        create_full_prefix_search_string,
        create_minimal_prefix_search_string,
    ],
    columns_not_null=[
        column.PROFESSIONAL_ID,
        column.MINIMAL_PREFIX_SEARCH,
        column.NAME,
        column.SURNAME,
        column.PROFESSIONAL_CATEGORY,
        column.PROFESSION,
    ],
    columns_unique=[
        column.PROFESSIONAL_ID,
        column.MINIMAL_PREFIX_SEARCH,
        column.FULL_PREFIX_SEARCH
    ],
    additional_columns_from_piped_transformations=[
        column.FULL_PREFIX_SEARCH,
        column.MINIMAL_PREFIX_SEARCH
    ],
)

uploader = Uploader(csv.HEALTH_PROFESSIONAL_CSV, table_name=tables.HEALTH_PROFESSIONAL_DIRECTORY)

SEARCH_COLUMNS = [
    column.FULL_PREFIX_SEARCH,
    column.MINIMAL_PREFIX_SEARCH,
    column.TOWN,
]
indices: index_list = [
    SearchIndexDDL(tables.HEALTH_PROFESSIONAL_DIRECTORY, column_name)
    for column_name in SEARCH_COLUMNS
]

INDEX_COLUMNS = [
    column.PROFESSION,
    column.SPECIALTY,
    column.PROFESSIONAL_CATEGORY,
]
indices += [
    IndexDDL(tables.HEALTH_PROFESSIONAL_DIRECTORY, column_name)
    for column_name in INDEX_COLUMNS
]
indices.append(IndexDDL(tables.HEALTH_PROFESSIONAL_DIRECTORY, column.PROFESSIONAL_ID, unique=True))

if __name__ == '__main__':
    # downloader.reset()
    downloader.run()
    extractor.reset()
    extractor.run()
    cleaner.reset()
    cleaner.run()
