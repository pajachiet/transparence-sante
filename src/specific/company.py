import logging
import os

import pandas as pd

from src.constants import tables, column
from src.constants.csv import COMPANY_CSV, COMMA_SEPARATOR, COMPANY_GROUP_CSV
from src.constants.directories import ROOTED_RAW_DATA_DIR
from src.generic.cleaner import Cleaner
from src.generic.ddl.index import SearchIndexDDL
from src.generic.uploader import Uploader

ADDRESS_LINE_1 = "adresse_1"
ADDRESS_LINE_2 = "adresse_2"
ADDRESS_LINE_3 = "adresse_3"
ADDRESS_LINE_4 = "adresse_4"
ADDRESS_LINES = [ADDRESS_LINE_1, ADDRESS_LINE_2, ADDRESS_LINE_3, ADDRESS_LINE_4]


def address(df: pd.DataFrame) -> pd.Series:
    s_address = df[ADDRESS_LINE_1].fillna('').str.rstrip()
    for line in ADDRESS_LINES[1:]:
        s_address += (', ' + df[line]).fillna('').str.rstrip()
    return s_address


def parent_company(df: pd.DataFrame) -> pd.DataFrame:
    csv_path = os.path.join(ROOTED_RAW_DATA_DIR, COMPANY_GROUP_CSV)
    source_column = column.SUBSIDIARY_COMPANY

    referential_df = (pd.read_csv(csv_path, sep=COMMA_SEPARATOR, dtype='str')
                      .dropna(subset=['identifiant']))

    check_missing_companies_from_referential(df, referential_df)

    mapping = (referential_df.set_index(column.COMPANY_NAME).to_dict()[column.COMPANY_GROUP])
    return (df[source_column]
            .map(mapping, na_action='ignore')
            .fillna(df[source_column])
            )


def check_missing_companies_from_referential(df: pd.DataFrame, referential_df: pd.DataFrame) -> None:
    mask_not_in_referential = ~df[column.SUBSIDIARY_COMPANY].isin(referential_df[column.COMPANY_NAME])
    nb_missing_in_referential = sum(mask_not_in_referential)
    if nb_missing_in_referential:
        logging.warning("{} companies are not present in association to parent company"
                        .format(nb_missing_in_referential))
        pd.options.display.max_columns = 10
        pd.options.display.max_rows = 1000
        logging.info("These are the missing companies.")
        df_missing_companies = (df.loc[mask_not_in_referential, [column.SUBSIDIARY_COMPANY, column.COMPANY_ID]]
                                .drop_duplicates()
                                .sort_values(column.SUBSIDIARY_COMPANY))
        logging.info(df_missing_companies)


cleaner = Cleaner(
    input_csv_name=COMPANY_CSV,
    input_separator=COMMA_SEPARATOR,
    keep_and_rename_columns={
        "identifiant": column.COMPANY_ID,
        "pays_code": column.COUNTRY_CODE,
        "pays": column.COUNTRY,
        "secteur": column.SECTOR,
        "denomination_sociale": column.SUBSIDIARY_COMPANY,
        "adresse_1": ADDRESS_LINE_1,
        "adresse_2": ADDRESS_LINE_2,
        "adresse_3": ADDRESS_LINE_3,
        "adresse_4": ADDRESS_LINE_4,
        "code_postal": column.POSTAL_CODE,
        "ville": column.CITY,
    },
    assign_columns_with_functions={
        column.ADDRESS: address,
        column.COUNTRY_CODE: lambda df: df[column.COUNTRY_CODE].str.lstrip('[').str.rstrip(']'),
        column.COMPANY: parent_company
    },
    columns_to_drop=ADDRESS_LINES
)

uploader = Uploader(COMPANY_CSV)

COMPANY_SEARCH_COLUMNS = [
    column.COUNTRY,
    column.SECTOR,
    column.COMPANY
]

indices = [
    SearchIndexDDL(tables.COMPANY_DIRECTORY, column_name)
    for column_name in COMPANY_SEARCH_COLUMNS
]

indices.append(SearchIndexDDL(tables.COMPANY_DIRECTORY, column.COMPANY_ID, unique=True))
