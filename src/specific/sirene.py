from src.constants.csv import SIRENE_URL, SIRENE_CSV, SIRENE_ZIP, COMMA_SEPARATOR, NAF_CSV, NAF_URL
from src.generic.cleaner import Cleaner
from src.generic.ddl.index import IndexDDL
from src.generic.downloader import Downloader, Extractor
from src.generic.uploader import Uploader

# SIRENE
sirene_downloader = Downloader(SIRENE_URL, SIRENE_ZIP)
sirene_extractor = Extractor(SIRENE_ZIP, SIRENE_CSV)

sirene_keep_and_rename = {
    'activiteprincipaleunitelegale': 'activiteprincipaleunitelegale',
    # 'anneecategorieentreprise': 'anneecategorieentreprise',
    # 'anneeeffectifsunitelegale': 'anneeeffectifsunitelegale',
    # 'caractereemployeurunitelegale': 'caractereemployeurunitelegale',
    'categorieentreprise': 'categorieentreprise',
    # 'categoriejuridiqueunitelegale': 'categoriejuridiqueunitelegale',
    # 'datecreationunitelegale': 'datecreationunitelegale',
    # 'datedebut': 'datedebut',
    'denominationunitelegale': 'denominationunitelegale',
    'denominationusuelle1unitelegale': 'denominationusuelle1unitelegale',
    # 'denominationusuelle2unitelegale': 'denominationusuelle2unitelegale',
    # 'denominationusuelle3unitelegale': 'denominationusuelle3unitelegale',
    # 'economiesocialesolidaireunitelegale': 'economiesocialesolidaireunitelegale',
    # 'etatadministratifunitelegale': 'etatadministratifunitelegale',
    # 'identifiantassociationunitelegale': 'identifiantassociationunitelegale',
    # 'nicsiegeunitelegale': 'nicsiegeunitelegale',
    # 'nombreperiodesunitelegale': 'nombreperiodesunitelegale',
    'nomenclatureactiviteprincipaleunitelegale': 'nomenclatureactiviteprincipaleunitelegale',
    'nomunitelegale': 'nomunitelegale',
    'nomusageunitelegale': 'nomusageunitelegale',
    'prenom1unitelegale': 'prenom1unitelegale',
    # 'prenom2unitelegale': 'prenom2unitelegale',
    # 'prenom3unitelegale': 'prenom3unitelegale',
    # 'prenom4unitelegale': 'prenom4unitelegale',
    'prenomusuelunitelegale': 'prenomusuelunitelegale',
    # 'pseudonymeunitelegale': 'pseudonymeunitelegale',
    # 'sexeunitelegale': 'sexeunitelegale',
    'sigleunitelegale': 'sigleunitelegale',
    'siren': 'siren',
    # 'statutdiffusionunitelegale': 'statutdiffusionunitelegale',
    # 'to_char': 'to_char',
    # 'trancheeffectifsunitelegale': 'trancheeffectifsunitelegale',
    # 'unitepurgeeunitelegale': 'unitepurgeeunitelegale'
}

sirene_cleaner = Cleaner(
    SIRENE_CSV,
    input_separator=COMMA_SEPARATOR,
    keep_and_rename_columns=sirene_keep_and_rename)

sirene_uploader = Uploader(
    SIRENE_CSV,
    table_name='sirene')

index_sirene = [
    IndexDDL('sirene', 'siren')
]

# NAF
naf_downloader = Downloader(NAF_URL, NAF_CSV)
naf_keep_and_rename = {
    'Code NAF': 'code_naf',
    'Intitule NAF': 'intitule_naf',
}
naf_cleaner = Cleaner(NAF_CSV, keep_and_rename_columns=naf_keep_and_rename)
naf_uploader = Uploader(NAF_CSV, table_name='naf_code')

if __name__ == '__main__':
    naf_downloader.run()
    naf_cleaner.run()
