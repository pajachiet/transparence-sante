from typing import List, Tuple

import pandas as pd


def concat_str_columns_from_df(df: pd.DataFrame, columns: List[str], sep: str = ' ') -> pd.Series:
    result_series = df[columns[0]]
    for column_name in columns[1:]:
        result_series = concat_two_str_series(result_series, df[column_name], sep=sep)
    return result_series


def concat_str_columns_from_df_with_separator(df: pd.DataFrame, first_column: str,
                                              column_sep_tuples: List[Tuple[str, str]]) -> pd.Series:
    result_series = df[first_column]
    for column_name, sep in column_sep_tuples:
        result_series = concat_two_str_series(result_series, df[column_name], sep=sep)
    return result_series


def concat_two_str_series(str_series_1: pd.Series, str_series_2: pd.Series, sep: str):
    return (str_series_1.fillna('')
            .str.cat(str_series_2.fillna('').values, sep=sep)
            .apply(rchop, args=(sep,))
            .str.strip()
            )


def rchop(s, ending):
    if s.endswith(ending):
        return s[:-len(ending)]
    return s