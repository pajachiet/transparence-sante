from os.path import join as p_join

from src.constants import column
from src.constants import tables
from src.constants.csv import REMUNERATION_CSV, ADVANTAGE_CSV, CONVENTION_CSV
from src.constants.directories import ROOTED_CLEANED_DATA_DIR, ROOTED_JOINED_DATA_DIR
from src.generic.ddl.view import View
from src.utils.dataframe_io import get_csv_columns
from src.utils.utils import quoted

DECLARATION_VIEW_TEMPLATE = """
CREATE OR REPLACE VIEW {name} AS (
    SELECT {remuneration_columns_formatted} FROM {declaration_remuneration}
    UNION ALL
    SELECT {convention_columns_formatted}  FROM {declaration_convention}
    UNION ALL
    SELECT {advantage_columns_formatted} FROM {declaration_advantage}
);
"""


def get_convention_columns():
    return get_csv_columns(p_join(ROOTED_JOINED_DATA_DIR, CONVENTION_CSV))


def get_advantage_columns():
    return get_csv_columns(p_join(ROOTED_CLEANED_DATA_DIR, ADVANTAGE_CSV))


def get_remuneration_columns():
    return get_csv_columns(p_join(ROOTED_CLEANED_DATA_DIR, REMUNERATION_CSV))


def get_columns_union():
    return sorted(
        set(get_remuneration_columns()) |
        set(get_convention_columns()) |
        set(get_advantage_columns())
    )


def formatted_columns(declaration_columns):
    formatted_list = []
    for column_name in get_columns_union():
        if column_name in declaration_columns:
            formatted_list.append(quoted(column_name))
        else:
            formatted_list.append("NULL as " + quoted(column_name))

    return ', '.join(formatted_list)


def view():
    return View(
        name=tables.DECLARATION,
        ddl_template=DECLARATION_VIEW_TEMPLATE,
        remuneration_columns_formatted=formatted_columns(get_remuneration_columns()),
        convention_columns_formatted=formatted_columns(get_convention_columns()),
        advantage_columns_formatted=formatted_columns(get_advantage_columns()),
        declaration_remuneration=tables.DECLARATION_REMUNERATION,
        declaration_convention=tables.DECLARATION_CONVENTION,
        declaration_advantage=tables.DECLARATION_ADVANTAGE,
        company=tables.COMPANY_DIRECTORY,
        company_id=column.COMPANY_ID,
        parent_company=column.COMPANY
    )
