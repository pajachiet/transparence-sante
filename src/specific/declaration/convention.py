import pandas as pd

from src.constants import column
from src.constants.categorical import CONVENTION
from src.constants.column import RAW_EVENT_NAME, RAW_EVENT_DATE, RAW_EVENT_PLACE, RAW_EVENT_ORGANIZER
from src.constants.csv import CONVENTION_CSV
from src.specific.declaration.common import DeclarationCleaner
from src.specific.declaration.functions import get_declaration_id_builder
from src.specific.normalize_functions import normalize_detail
from src.utils.utils import merge_dict

OTHER = 'Autre'
EVENT = 'Manifestation'
ORGANIZED_BY = 'Organisé le'
AT = 'À'
BY = 'Par'


def get_column_formatter(prefix, str_format ="{} : '{}' "):
    def format_value(value):
        if value is None or pd.isna(value):
            return ''
        else:
            return str_format.format(prefix, value)

    return format_value


def event_detail(df):
    return (df[RAW_EVENT_NAME].map(get_column_formatter(EVENT)) +
            df[RAW_EVENT_DATE].map(get_column_formatter(ORGANIZED_BY)) +
            df[RAW_EVENT_PLACE].map(get_column_formatter(AT)) +
            df[RAW_EVENT_ORGANIZER].map(get_column_formatter(BY))
            )


def convention_object(df):
    return df[column.RAW_CONVENTION_OBJECT] \
        .where(df[column.RAW_CONVENTION_OBJECT] != OTHER, df[column.RAW_CONVENTION_OBJECT_OTHER])


def convention_date(df):
    return (df[column.RAW_CONVENTION_START_DATE].map(get_column_formatter('Début contrat', '{} {}')) +
            df[column.RAW_CONVENTION_END_DATE].map(get_column_formatter(' Fin contrat', '{} {}'))
            )


def convention_detail(df: pd.DataFrame) -> pd.Series:
    return (
            convention_object(df).pipe(normalize_detail).str.replace('|', '-') +
            ' | ' + event_detail(df).str.replace('|', '-') +
            ' | ' + convention_date(df).str.replace('|', '-')
    )


def replace_zero_amount_by_none(df):
    return df[column.AMOUNT].replace(to_replace='0', value=pd.np.NaN)


cleaner = DeclarationCleaner(
    input_csv_name=CONVENTION_CSV,
    keep_and_rename_columns=merge_dict([
        {
            'conv_date_signature': column.DATE,
            'conv_montant_ttc': column.AMOUNT,
        },
        {column: column for column in column.RAW_COLUMNS_CONVENTION}
    ]),
    assign_columns_with_functions={
        column.DECLARATION_TYPE: CONVENTION,
        column.DETAIL: convention_detail,
        column.PRECISE_CATEGORY: None,
        column.GENERAL_CATEGORY: None,
        column.AMOUNT: replace_zero_amount_by_none,
        column.DECLARATION_ID: get_declaration_id_builder(CONVENTION, column.LINE_ID),
        column.CONVENTION_ID: get_declaration_id_builder(CONVENTION, column.LINE_ID)
    },
    columns_to_drop=column.RAW_COLUMNS_CONVENTION,
    columns_unique=[column.DECLARATION_ID]
)

if __name__ == '__main__':
    # cleaner.profile_raw_csv()
    cleaner.reset()
    cleaner.run()
