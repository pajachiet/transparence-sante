from datetime import datetime

from src.constants import column
from src.generic.validate import ValidationError
from src.specific.normalize_functions import normalize_name, normalize_surname
from src.specific.utils import concat_str_columns_from_df

CURRENT_YEAR = datetime.now().year

MINIMAL_YEAR = 1970
MAXIMAL_YEAR = CURRENT_YEAR
MINIMAL_YEAR_DECADE_19XX = 70
MAXIMAL_YEAR_DECADE_20XX = CURRENT_YEAR - 2000


def build_address(df):
    return concat_str_columns_from_df(df,
                                      ["benef_adresse1", "benef_adresse2", "benef_adresse3", "benef_adresse4"],
                                      sep=', ')


def concat_name_surname(df):
    name = normalize_name(df)
    surname = normalize_surname(df)
    return (name
            .str.cat(surname.values, sep=' ')
            .str.strip()
            )


def corrected_date(df):
    def corrected_year(year):
        year_decade = year[2:]
        if MINIMAL_YEAR_DECADE_19XX <= int(year_decade):
            return '19' + year_decade
        elif int(year_decade) <= MAXIMAL_YEAR_DECADE_20XX:
            return '20' + year_decade
        else:
            return '2000'

    s_date = df[column.DATE]
    s_day_month = s_date.str[:6]
    s_year = s_date.str[6:].map(corrected_year)
    return s_day_month + s_year


def validate_year_is_in_correct_range(df):
    def is_date_string_year_in_correct_range(date_string):
        year = int(date_string[6:])
        return MINIMAL_YEAR <= year <= MAXIMAL_YEAR

    if not df[column.DATE].map(is_date_string_year_in_correct_range).all():
        raise ValidationError("Some date are not in correct range")


def get_declaration_id_builder(declaration_type, declaration_id_column):
    def build_declaration_id(df):
        return (declaration_type + ' | ' +
                df[column.COMPANY_ID] + ' | ' +
                df[declaration_id_column]
                )

    return build_declaration_id
