import logging
import os

from dotenv import load_dotenv

load_dotenv()
LOGGING_LEVEL = os.getenv('LOGGING_LEVEL', 'INFO')
if LOGGING_LEVEL not in logging._nameToLevel:
    raise ValueError("LOGGING_LEVEL='{}' bug it should be in list [{}]"
                     .format(LOGGING_LEVEL, ','.join(logging._nameToLevel.keys())))
LOGGING_LEVEL = logging._nameToLevel[LOGGING_LEVEL]

logging.basicConfig(level=LOGGING_LEVEL,
                    format='%(asctime)s :: %(levelname)s :: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    )
