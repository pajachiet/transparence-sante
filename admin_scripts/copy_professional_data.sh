#!/usr/bin/env bash

host=${1:-"ts-prod"}

scp data/cleaned/activite_professionnel_sante.csv root@${host}:/root/transparence-sante/data/cleaned/activite_professionnel_sante.csv
scp data/cleaned/professionnel_sante.csv root@${host}:/root/transparence-sante/data/cleaned/professionnel_sante.csv
scp data/cleaned/etablissement_sante.csv root@${host}:/root/transparence-sante/data/cleaned/etablissement_sante.csv


