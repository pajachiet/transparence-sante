#!/usr/bin/env bash

host=${1:-"ts-prod"}

ssh -t root@${host} "cd transparence-sante && git pull && docker-compose build frontend && docker-compose up -d frontend nginx"
