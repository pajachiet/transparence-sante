#!/usr/bin/env bash
set -euo pipefail

dump_file=$1

# Stop metabase
docker-compose stop metabase
docker-compose rm metabase

# (Re)start fresh mb-postgres
docker-compose stop mb-postgres
docker-compose rm mb-postgres

echo You are going to delete all data in mb-posgres
read -p "Continue (I confirm/ No)? " choice
case "$choice" in
  "I confirm") echo "Yes. Going on" ;;
  n|N|no|No ) echo "No. Aborting" ; exit ;;
  * ) echo "invalid. Aborting" ; exit ;;
esac

rm -rf docker/mb-postgres/data

docker-compose up -d mb-postgres
echo Waiting 20s for postgres to start
echo After we will restore Metabase dump from ${dump_file} to localhost
sleep 20


# Restore backup
echo Stop sleeping, start backuping
source .env
PGPASSWORD=${MB_POSTGRES_PASSWORD} psql --set ON_ERROR_STOP=on postgres -h localhost -p 6543 -U postgres < ${dump_file}


# Start metabase on postgres
docker-compose up -d metabase
docker-compose restart nginx