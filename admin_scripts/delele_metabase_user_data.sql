DELETE FROM activity WHERE user_id <> 1;

DELETE FROM report_dashboardcard
WHERE dashboard_id iN (SELECT id
                       FROM report_dashboard
                       WHERE creator_id <> 1
                          OR made_public_by_id <> 1);


DELETE FROM report_dashboard WHERE creator_id <> 1;
DELETE FROM report_dashboard WHERE made_public_by_id <> 1;


DELETE FROM pulse WHERE creator_id <> 1;
DELETE FROM report_card WHERE creator_id <> 1;
DELETE FROM report_card
WHERE collection_id IN (SELECT id
                        FROM collection
                        WHERE personal_owner_id <> 1);

DELETE FROM collection WHERE personal_owner_id <> 1;
DELETE FROM collection_revision WHERE user_id <> 1;
DELETE FROM computation_job WHERE creator_id <> 1;
DELETE FROM core_session WHERE user_id <> 1;
DELETE FROM metric WHERE creator_id <> 1;
DELETE FROM permissions_group_membership WHERE user_id <> 1;
DELETE FROM permissions_revision WHERE user_id <> 1;
DELETE FROM pulse_channel_recipient WHERE user_id <> 1;
DELETE FROM report_cardfavorite WHERE owner_id <> 1;

DELETE FROM dashboard_favorite WHERE user_id <> 1;
DELETE FROM revision WHERE user_id <> 1;

DELETE FROM view_log WHERE  user_id <> 1;

DELETE FROM core_user WHERE id <> 1;

TRUNCATE query_cache;
TRUNCATE query_execution;


/* Connect as admin then modify settings to remove keys for smtp, slack, etc */
