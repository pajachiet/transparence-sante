#!/usr/bin/env bash

host=${1:-"ts-prod"}

ssh -t root@${host} "cd transparence-sante && docker-compose logs --follow metabase"
