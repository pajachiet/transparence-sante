### Problem to solve

(Summarize the problem to solve concisely)

### Further details

(Include use cases, benefits, and/or goals)

### Proposal

(Include ideas and action plans to solve the problem)

### What does success look like, and how can we measure that?

(If no way to measure success, link to an issue that will implement a way to measure this)

### Links / references

