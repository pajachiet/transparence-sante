#!/usr/bin/env python3
import sys
from typing import List

from settings import POSTGRES_HOST, PROCESS_REFERENCE_DIRECTORIES
from src.constants.connexions import LOCALHOST, DOCKER_POSTGRES_HOST
from src.constants.miscellaneous import ERROR_CODE_INVALID_ARGUMENT
from src.generic.cleaner import Cleaner
from src.generic.ddl.foreign_key import ForeignKeyDDL
from src.generic.downloader import Downloader, Extractor
from src.generic.dumper import Dumper
from src.generic.uploader import Uploader
from src.metabase.api import wait_for_metabase, query_main_cards
from src.metabase.cache import delete_metabase_cache
from src.specific import company
from src.specific.company_group import add_missing_companies_to_group, company_group_downloader
from src.specific.declaration import duplicate_amounts_advantage_convention
from src.specific.declaration import remuneration, advantage, convention, common as declaration, \
    view as declaration_view, join_convention_to_other_declarations
from src.specific.health_directoy import health_professional, health_establishment, health_professional_activities
from src.specific.sirene import naf_downloader, naf_cleaner, naf_uploader
from src.specific.sunshine_europe import dump_to_sunshine_europe_format
from src.specific.types import index_list, view_list, upload_actions_list
from src.utils.postgres import wait_for_postgres, create_trgm_extension
from src.utils.utils import get_confirmation


def all_downloader() -> List[Downloader]:
    result = [
        company_group_downloader,
        declaration.downloader,
    ]
    if PROCESS_REFERENCE_DIRECTORIES:
        result += [
            health_professional.downloader,
            # sirene_downloader,
            naf_downloader
        ]
    return result


def download(reset: bool = False) -> None:
    for downloader in all_downloader():
        if reset:
            downloader.reset()
        downloader.run()


def all_extractors() -> List[Extractor]:
    result = declaration.extractor_list
    if PROCESS_REFERENCE_DIRECTORIES:
        result += [
            health_professional.extractor,
            # sirene_extractor
        ]
    return result


def extract(reset: bool = False) -> None:
    for extractor in all_extractors():
        if reset:
            extractor.reset()
        extractor.run()


def all_cleaners() -> List[Cleaner]:
    result = [
        remuneration.cleaner,
        advantage.cleaner,
        convention.cleaner,
        company.cleaner,
        join_convention_to_other_declarations.cleaner
    ]
    if PROCESS_REFERENCE_DIRECTORIES:
        result += [
            health_professional.cleaner,
            health_establishment.cleaner,
            health_professional_activities.cleaner,
            # sirene_cleaner,
            naf_cleaner
        ]
    return result


def clean(reset: bool = False) -> None:
    for cleaner in all_cleaners():
        if reset:
            cleaner.reset()
        cleaner.run()


def all_uploaders() -> List[Uploader]:
    result = [
        company.uploader,
        remuneration.uploader,
        advantage.uploader,
        join_convention_to_other_declarations.uploader,
    ]
    if PROCESS_REFERENCE_DIRECTORIES:
        result += [
            health_professional.uploader,
            health_establishment.uploader,
            health_professional_activities.uploader,
            # sirene_uploader,
            naf_uploader
        ]
    return result


def all_views_ddl() -> view_list:
    [duplicate_amounts_advantage_convention.materialized_view]
    return ([declaration_view.view()] +
            declaration.materialized_views)


def all_indices() -> index_list:
    result = (
            declaration.indices +
            company.indices
    )
    if PROCESS_REFERENCE_DIRECTORIES:
        result += (
                health_establishment.indices +
                health_professional.indices +
                health_professional_activities.indices  # +
            # index_sirene
        )
    return result


def all_foreign_keys() -> List[ForeignKeyDDL]:
    result = declaration.foreign_keys
    if PROCESS_REFERENCE_DIRECTORIES:
        result += health_professional_activities.foreign_keys
    return result


def all_upload_actions() -> upload_actions_list:
    result: upload_actions_list = all_uploaders()
    return result + all_views_ddl() + all_indices() + all_foreign_keys()


def upload(reset: bool = False) -> None:
    wait_for_postgres()
    create_trgm_extension()
    if reset:
        for uploader in all_uploaders():
            uploader.reset()
    for upload_action in all_upload_actions():
        upload_action.run()


def update_ddl(reset: bool = True) -> None:
    for uploader in all_uploaders():
        uploader.table_ddl.write_ddl()
    for view in all_views_ddl():
        view.write_ddl()


def load_metabase_cache(reset: bool = False) -> None:
    wait_for_metabase()
    if reset:
        delete_metabase_cache()
    query_main_cards()


def dump(reset: bool = False) -> None:
    for uploader in all_uploaders():
        dumper = Dumper(directory=uploader.csv_directory, file_name=uploader.csv_name)
        if reset:
            dumper.reset()
        dumper.run()


def process(reset: bool = False) -> None:
    extract(reset)
    clean(reset)
    upload(reset)
    load_metabase_cache(reset)
    dump(reset)
    dump_to_sunshine_europe_format(reset)


ACTION_MAPPING = {
    'download': download,
    'extract': extract,
    'clean': clean,
    'upload': upload,
    'load-cache': load_metabase_cache,
    'dump': dump,
    'process': process,
    'company_group': add_missing_companies_to_group,
    'update_ddl': update_ddl,
    'sunshine_europe': dump_to_sunshine_europe_format
}


def invalid_input():
    print("Main entrypoint to use eurosfordocs project\n")
    print("Usage:")
    for action_key in ACTION_MAPPING.keys():
        print("  ./main.py {} [-r|--reset]".format(action_key))
    exit(ERROR_CODE_INVALID_ARGUMENT)


if __name__ == "__main__":
    if len(sys.argv) not in [2, 3]:
        invalid_input()

    action = sys.argv[1]
    if action not in ACTION_MAPPING:
        invalid_input()

    reset_arg = False
    if len(sys.argv) == 3:
        reset_arg = True
        if sys.argv[2] not in ['-r', '--reset']:
            invalid_input()

    if POSTGRES_HOST not in [LOCALHOST, DOCKER_POSTGRES_HOST]:
        get_confirmation(
            "You are going to execute action '{}' with POSTGRES_HOST='{}'.\nDo you confirm ?"
                .format(action, POSTGRES_HOST)
        )

    ACTION_MAPPING[action](reset_arg)
