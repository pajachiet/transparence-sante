import string

import numpy as np
import pandas as pd
import pytest

from src.constants import column
from src.generic.cleaner_functions import to_ascii_lowercase, get_non_authorized_chars_remover, replace_accents
from src.specific.declaration.convention import replace_zero_amount_by_none
from src.specific.declaration.functions import concat_name_surname
from src.specific.normalize_functions import normalize_string_series


def test_to_ascii_lowercase():
    # Given
    input_string = \
        'ÉÈÊËEĘEĒ éèêëęeė àâä ùûü òôö  îiïiíiį ç 1{}-='
    expected_cleaned_string = \
        'eeeeeeee eeeeeee aaa uuu ooo iiiiiii c'

    # When
    cleaned_string = to_ascii_lowercase(input_string)

    # Then
    assert expected_cleaned_string == cleaned_string


def test_replace_accents():
    # Given
    accentuated_string = \
        'ÉÈÊËEĘEĒ éèêëęeė àâä ùûü òôö îiïiíiį ç'
    expected_cleaned_string = \
        'EEEEEEEE eeeeeee aaa uuu ooo iiiiiii c'

    # When
    cleaned_string = replace_accents(accentuated_string)

    # Then
    assert expected_cleaned_string == cleaned_string


def test_replace_special_characters():
    # Given
    special_character_string = \
        'aeiouAEIOU &@#-'
    expected_cleaned_string = \
        'aeiouAEIOU     '

    # When
    cleaned_string = get_non_authorized_chars_remover(string.ascii_letters)(special_character_string)

    # Then
    assert expected_cleaned_string == cleaned_string


raw_to_expected_string = [
    ('', ''),
    ('aà', 'aa'),
    (np.nan, ''),
    (' a \n b ', 'a b'),
    ('a-b', 'a b'),
    ("a ' b", "a b")
]


@pytest.mark.parametrize('raw_string,expected_string', raw_to_expected_string)
def test_normalize_string_series(raw_string, expected_string):
    # Given
    raw_series = pd.Series([raw_string])
    expected_series = pd.Series([expected_string])

    # When
    actual_series = normalize_string_series(raw_series)

    # Then
    assert expected_series[0] == actual_series[0]
    assert actual_series.equals(expected_series)


name_surname_list = [
    ('', '', ''),
    (np.NaN, np.NaN, ''),
    ('name', np.NaN, 'NAME'),
    (' name ', '', 'NAME'),
    ('name', 'surname', 'NAME Surname'),
    ('nàMé', 'sürnÀmé', 'NAME Surname'),
]


@pytest.mark.parametrize('name,surname,name_surname', name_surname_list)
def test_recipient_name_surname(name, surname, name_surname):
    # Given
    df = pd.DataFrame(
        data=[[name, surname]],
        columns=[column.NAME, column.SURNAME]
    )
    expected_name_surname = pd.Series([name_surname])

    # When
    actual_name_surname_series = concat_name_surname(df)

    # Then
    assert actual_name_surname_series.equals(expected_name_surname)


amount_list = [
    ('', ''),
    ('0', np.NaN),
    ('a', 'a'),
    (0, 0)
]


@pytest.mark.parametrize('amount,replaced_amount', amount_list)
def test_replace_zero_amount_by_none(amount, replaced_amount):
    # Given
    df = pd.DataFrame(
        data=[[amount]],
        columns=[column.AMOUNT]
    )
    expected_amount = pd.Series([replaced_amount])

    # When
    actual_amount = replace_zero_amount_by_none(df)

    # Then
    assert actual_amount.equals(expected_amount)
