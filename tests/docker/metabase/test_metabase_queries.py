import logging

import pytest

from main import all_upload_actions
from settings import METABASE_BASE_URL
from src.metabase import api


@pytest.fixture(scope="module")
def setup_postgres_with_test_data():
    for action in all_upload_actions():
        action.reset()
        action.run()


token = api.metabase_get_token()
all_cards = api.metabase_get("/api/card/", token)


@pytest.mark.parametrize("card", sorted(all_cards, key=lambda x: x['id']))
def test_query_card(setup_postgres_with_test_data, card):
    logging.info(card['id'])
    result = api.metabase_query_card(card['id'], token, ignore_cache=True)
    dashboard_urls = []
    if result['status'] != 'completed':
        dashboard_urls = ["{}/dashboard/{}".format(METABASE_BASE_URL, dashboard_id)
                          for dashboard_id in api.find_dashboards_containing_card(card['id'], token)]

    assert result['status'] == 'completed', \
        "{error}\n" \
        "Question n° {id}, named '{name}' failed.\n" \
        "It can be accessed at {url}/question/{id}.\n" \
        "It is used in the following dashboards :\n - {dashboards_urls}" \
            .format(id=card['id'], error=result['error'], name=card['name'], url=METABASE_BASE_URL,
                    dashboards_urls="\n - ".join(dashboard_urls))
