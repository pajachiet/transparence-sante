import pytest

from src.utils.postgres import create_trgm_extension
from main import all_upload_actions


create_trgm_extension()


@pytest.mark.parametrize("action", all_upload_actions())
def test_postgres_actions(action):
    action.reset()
    action.run()
