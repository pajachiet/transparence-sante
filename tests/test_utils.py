import os

from src.utils.utils import add_final_directory_to_file_path


def test_add_final_directory_to_file_path():
    # Given
    file_path = os.path.join("foo", "file.csv")
    intermediate_directory = "bar"
    expected_path = os.path.join("foo", "bar", "file.csv")

    # When
    actual_path = add_final_directory_to_file_path(file_path, intermediate_directory)

    # Then
    assert expected_path == actual_path