
SET work_mem = '150MB'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW "declaration_sum_montant_ttc_by_entreprise_émmetrice" AS (
    SELECT
        "entreprise_émmetrice",
        sum("montant_ttc") AS "sum_montant_ttc"
    FROM "declaration"
    WHERE (
        ("entreprise_émmetrice" IS NOT NULL)
        AND ("montant_ttc" IS NOT NULL)
    )
    GROUP BY "entreprise_émmetrice"
    ORDER BY (sum("montant_ttc")) DESC
);
RESET work_mem;

CREATE INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_entreprise_émmetrice_trgm_gin_index_on_entreprise_émmetrice"
ON public."declaration_sum_montant_ttc_by_entreprise_émmetrice" USING GIN (lower("entreprise_émmetrice") gin_trgm_ops);

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_entreprise_émmetrice_index_on_entreprise_émmetrice"
ON public."declaration_sum_montant_ttc_by_entreprise_émmetrice" ("entreprise_émmetrice");

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_entreprise_émmetrice_index_on_sum_montant_ttc"
ON public."declaration_sum_montant_ttc_by_entreprise_émmetrice" ("sum_montant_ttc");
