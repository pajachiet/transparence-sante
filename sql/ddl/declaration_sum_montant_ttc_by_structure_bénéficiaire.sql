
SET work_mem = '150MB'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW "declaration_sum_montant_ttc_by_structure_bénéficiaire" AS (
    SELECT
        "structure_bénéficiaire",
        sum("montant_ttc") AS "sum_montant_ttc"
    FROM "declaration"
    WHERE (
        ("structure_bénéficiaire" IS NOT NULL)
        AND ("montant_ttc" IS NOT NULL)
    )
    GROUP BY "structure_bénéficiaire"
    ORDER BY (sum("montant_ttc")) DESC
);
RESET work_mem;

CREATE INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_structure_bénéficiaire_trgm_gin_index_on_structure_bénéficiaire"
ON public."declaration_sum_montant_ttc_by_structure_bénéficiaire" USING GIN (lower("structure_bénéficiaire") gin_trgm_ops);

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_structure_bénéficiaire_index_on_structure_bénéficiaire"
ON public."declaration_sum_montant_ttc_by_structure_bénéficiaire" ("structure_bénéficiaire");

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_structure_bénéficiaire_index_on_sum_montant_ttc"
ON public."declaration_sum_montant_ttc_by_structure_bénéficiaire" ("sum_montant_ttc");
