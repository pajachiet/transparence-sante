
CREATE TABLE "naf_code" (
    "id" SERIAL PRIMARY KEY,
    "code_naf" TEXT,
    "intitule_naf" TEXT
);
