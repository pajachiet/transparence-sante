
CREATE TABLE "activite_professionnel_sante" (
    "id" SERIAL PRIMARY KEY,
    "identifiant" TEXT,
    "identifiant_organisation" TEXT,
    "mode_exercice" TEXT
);
