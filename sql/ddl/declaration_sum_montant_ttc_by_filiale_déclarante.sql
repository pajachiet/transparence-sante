
SET work_mem = '150MB'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW "declaration_sum_montant_ttc_by_filiale_déclarante" AS (
    SELECT
        "filiale_déclarante",
        sum("montant_ttc") AS "sum_montant_ttc"
    FROM "declaration"
    WHERE (
        ("filiale_déclarante" IS NOT NULL)
        AND ("montant_ttc" IS NOT NULL)
    )
    GROUP BY "filiale_déclarante"
    ORDER BY (sum("montant_ttc")) DESC
);
RESET work_mem;

CREATE INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_filiale_déclarante_trgm_gin_index_on_filiale_déclarante"
ON public."declaration_sum_montant_ttc_by_filiale_déclarante" USING GIN (lower("filiale_déclarante") gin_trgm_ops);

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_filiale_déclarante_index_on_filiale_déclarante"
ON public."declaration_sum_montant_ttc_by_filiale_déclarante" ("filiale_déclarante");

CREATE  INDEX IF NOT EXISTS "declaration_sum_montant_ttc_by_filiale_déclarante_index_on_sum_montant_ttc"
ON public."declaration_sum_montant_ttc_by_filiale_déclarante" ("sum_montant_ttc");
