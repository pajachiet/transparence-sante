
CREATE TABLE "sirene" (
    "id" SERIAL PRIMARY KEY,
    "activiteprincipaleunitelegale" TEXT,
    "categorieentreprise" TEXT,
    "denominationunitelegale" TEXT,
    "denominationusuelle1unitelegale" TEXT,
    "nomenclatureactiviteprincipaleunitelegale" TEXT,
    "nomunitelegale" TEXT,
    "nomusageunitelegale" TEXT,
    "prenom1unitelegale" TEXT,
    "prenomusuelunitelegale" TEXT,
    "sigleunitelegale" TEXT,
    "siren" TEXT
);
