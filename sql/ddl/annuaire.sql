
CREATE TABLE "annuaire" (
    "id" SERIAL PRIMARY KEY,
    "categorie_professionnelle" TEXT,
    "civilite" TEXT,
    "civilite_exercice" TEXT,
    "commune" TEXT,
    "identifiant" TEXT,
    "nom" TEXT,
    "nom_prénom" TEXT,
    "prefix_search_string_full" TEXT,
    "prefix_search_string_minimal" TEXT,
    "profession" TEXT,
    "prénom" TEXT,
    "specialité" TEXT,
    "type_identifiant" TEXT
);
