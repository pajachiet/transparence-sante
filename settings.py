import os

from dotenv import load_dotenv

from src.constants.connexions import get_default_postgres_host, get_default_metabase_host, \
    get_default_mb_postgres_host, get_default_mb_postgres_port
from src.utils.utils import env_flag

load_dotenv()

PROCESS_REFERENCE_DIRECTORIES = env_flag('PROCESS_REFERENCE_DIRECTORIES', default=True)

# Only for development
ROWS_LIMIT = int(os.getenv('ROWS_LIMIT', 10 ** 9))

# Only for tests. Defined in pytest.ini
DATA_ROOT_DIR = os.getenv('DATA_ROOT_DIR', '')

# Memory related
POSTGRES_WORK_MEM = os.getenv('POSTGRES_WORK_MEM', '150MB')
CSV_CHUNK_SIZE = int(os.getenv('CSV_CHUNK_SIZE', 10 ** 5))

# Postgres
POSTGRES_HOST = os.getenv('POSTGRES_HOST', get_default_postgres_host())
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', '')
POSTGRES_PORT = int(os.getenv('POSTGRES_PORT', '5432'))

# Metabase
METABASE_USERNAME = os.getenv('METABASE_USERNAME')
METABASE_PASSWORD = os.getenv('METABASE_PASSWORD')
METABASE_HOST = os.getenv('METABASE_HOST', get_default_metabase_host())
METABASE_BASE_URL = os.getenv('METABASE_BASE_URL', "http://{}:3000".format(METABASE_HOST))

# Metabase's Postgres backend
MB_POSTGRES_HOST = get_default_mb_postgres_host()
MB_POSTGRES_PORT = get_default_mb_postgres_port()
MB_POSTGRES_PASSWORD = os.getenv('MB_POSTGRES_PASSWORD', '')
