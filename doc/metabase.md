# Definition

This document explains how to setup a freshly started Metabase instance.

(This is work in progress, to be continued) 

## Settings

At [http://localhost:3000/admin/settings/general](http://localhost:3000/admin/settings/general): 
- Change the settings `FRIENDLY TABLE AND FIELD NAMES` to `Only replace underscores`, as it create inconsistent field names.


## Data model

At [http://localhost:3000/admin/settings/general/admin/datamodel/database](http://localhost:3000/admin/settings/general), set every column you want to filter on to the `Category` type.
