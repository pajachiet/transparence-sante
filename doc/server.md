
This document is mainly a memo on how to setup a new virtual machine to host the project.


## Server administration tips  

### From PC

- Setup ssh without password : ssh-copy-id
- Add an alias in /etc/hosts to server ip

### On server

- htop

    sudo apt-get install htop
   

## Server installation

- [Install Docker](https://docs.docker.com/install/linux/docker-ce/debian/#install-docker-ce-1)
- [Install Docker-Compose](https://docs.docker.com/compose/install/#install-compose)
- [Install git](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-debian-8)

- Clone and install project for a docker usage.


    git clone https://gitlab.com/eurosfordocs/transparence-sante.git
    cd transparence-sante
    cp .env-template .env  # Create a default .env file from template


- Edit `.env` file with secrets value

- Install project
    
    
    docker-compose up -d


## Setup GitLab Runners

Follow [installation](https://docs.gitlab.com/runner/install/linux-manually.html#install), 
but with root user

    sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
    
    sudo chmod +x /usr/local/bin/gitlab-runner
         
    sudo gitlab-runner install --user=root
    
    sudo gitlab-runner start


[Configuration](https://docs.gitlab.com/runner/register/index.html#gnulinux), 
with token from Runner section of project's 
[CI-CD settings](https://gitlab.com/eurosfordocs/transparence-sante/settings/ci_cd). 

     sudo gitlab-runner register

## Setup postgres
Connect to postgres

    docker-compose run --rm postgres sh
    psql --set ON_ERROR_STOP=on postgres -h postgres -p 5432 -U postgres


### Create a Datadog user on postgres to allow monitoring 

    create user datadog with password '<PASSWORD>';
    grant pg_monitor to datadog;

### setup postgres with trgm extension

    create extension pg_trgm;
   