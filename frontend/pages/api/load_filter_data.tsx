import fetch from 'isomorphic-unfetch';

import {NextApiRequest, NextApiResponse} from "next";
import {dashboardConfigs, DashboardSlug, parseSlug} from "../../helpers/dashboard_data";


async function queryMetabase(slug: DashboardSlug, fieldId: string, prefix: string) {
    const LIMIT = 100;

    const dashboardConfig = dashboardConfigs.get(slug);
    if (dashboardConfig === undefined) {
        throw new Error('invalid dashboard slug: ' + slug);
    }

    const url = (`${process.env.FRONTEND_METABASE_SITE_URL}/api/public/dashboard/${dashboardConfig.uuid}`
        + `/field/${fieldId}/search/${fieldId}`
        + `?value=${prefix}&limit=${LIMIT}`);

    const response = await fetch(url, {mode: 'no-cors'});
    return await response.json();
}


export default async (req: NextApiRequest, res: NextApiResponse) => {
    let params;
    try {
        params = JSON.parse(req.body);
    } catch(e) {
        res.status(400).json({'msg': `invalid json`});
        return;
    }
    const slug = parseSlug(params.slug);
    const fieldId = params.fieldId;
    const prefix = params.prefix;

    if (!slug) {
        res.status(400).json({'msg': `invalid dashboard slug: ${params.slug}`});
        return;
    }

    if (!fieldId) {
        res.status(400).json({'msg': 'missing fieldId'});
        return;
    }

    if (!prefix) {
        res.status(400).json({'msg': 'missing prefix'});
        return;
    }

    let [metabaseResults, metabaseError] = await handleError(queryMetabase(slug, fieldId, prefix));
    if (metabaseError) {
        console.error(metabaseError);
        console.error(`Failed to get metabase data for prefix=${prefix}`);
        res.status(400).json({'msg': 'error with this prefix'});
        return;
    }
    res.json(metabaseResults.map((tuple: string[]) => tuple[0]));

}


function handleError<T>(promise: Promise<T>) {
    return promise
        .then(data => ([data, null]))
        .catch(error => Promise.resolve([null, error]));
}
