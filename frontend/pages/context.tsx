import React from "react";
import ContentPage from "../components/content_page";

const pageText = `
## La transparence, pour éviter un nouveau Médiator

En 2010, le scandale du [Mediator](https://fr.wikipedia.org/wiki/Affaire_du_Mediator) éclate. 
Faisant suite à une série de crises sanitaires, il révèle des _failles majeures*_ dans toute la chaîne de sécurité des médicaments.

La _corruption de l'expertise sanitaire*_ par le lobby pharmaceutique apparaît comme une cause récurrente de ces dysfonctionnements.

Le 29 décembre 2011, la _loi post-Médiator*_, dite Bertrand, est promulguée. 

Pour lutter contre les conflits d'intérêts, sa disposition phare est de rendre transparents les liens entre industriels et professionnels de santé. Désormais, les entreprises devront déclarer le détail des dons, subventions et contrats sur un site internet public. 

Le 26 juin 2014, 4 ans après le scandale du Médiator, la base de données Transparence-Santé est officiellement lancée. 


<div class="ts-notes"> <strong>Notes :</strong>
    <ul>
        <li>
            <strong>Failles majeures :</strong> 
            Rapport du Sénat, <a href="http://www.senat.fr/rap/r10-675-1/r10-675-18.html#toc162">partie 1.II</a>: UNE SITUATION RÉVÉLATRICE DES DYSFONCTIONNEMENTS DU DISPOSITIF DE SÉCURITÉ SANITAIRE
        </li>
        <li>
            <strong>Corruption de l'expertise sanitaire :</strong>
            "le domaine sanitaire est particulièrement exposé au risque de conflits d'intérêts en raison d'enjeux financiers considérables et du lobbying des laboratoires pharmaceutiques." dans Rapport du Sénat, <a href="http://www.senat.fr/rap/r10-675-1/r10-675-19.html#toc246">partie 1.II.B.2.b</a> L'indépendance de l'expertise sanitaire en question
        </li>
        <li>
            <strong>Loi post-Médiator :</strong>
            "Car ce n’est pas la loi Bertrand, mais la loi post-Mediator, pour qu’il n’y ait plus de scandale de ce type." <a href="http://www.liberation.fr/societe/2011/09/27/ce-n-est-pas-la-loi-bertrand-mais-la-loi-post-mediator_763882">Interview de Xavier Bertrand</a> au journal Libération le 27 septembre 2011, qui expose clairement le projet de loi.
        </li>
        
    </ul>
</div>

## Les problèmes de Transparence-Santé

Depuis 2014, la base Transparence-Santé est alimentée tous les 6 mois par les industriels. 
Ces données détaillées *pourraient* être "un puissant outil d'assainissement des pratiques des entreprises" ([Rapport de la cour des comptes de 2016, p65](https://www.ccomptes.fr/sites/default/files/EzPublish/20160323-prevention-conflits-interets-en-matiere-expertise-sanitaire.pdf#page=65)).

Malheureusement, la transparence escomptée est limitée en pratique :

1. Le [moteur de recherche public](https://www.transparence.sante.gouv.fr/flow/interrogationAvancee?execution=e2s1) est **difficile d'emploi**.

   Lister les déclarations concernant un médecin nécessite plusieurs étapes manuelles fastidieuses.
   
1. Le site ne propose **aucune vision statistique**.

   Il est impossible d'avoir une vision d'ensemble des déclarations d'une entreprise ou concernant un professionnel. 
   
   La [cour des comptes](https://www.ccomptes.fr/sites/default/files/EzPublish/20160323-prevention-conflits-interets-en-matiere-expertise-sanitaire.pdf#page=65) pointe le problème, 
   qui est également [reconnu par le syndicat des entreprises pharmaceutiques](https://www.vrai-faux.leem.org/post/167273340191/non-les-nouvelles-r%C3%A8gles-de-transparence-ne), 
   "de la pédagogie mais également des améliorations du site sont aujourd’hui indispensables pour permettre à la transparence d’atteindre pleinement ses objectifs." 
   
1. Les déclarations sont de **mauvaise qualité**.
   
   Les déclarations transmises par les industriels sont hétérogènes et incomplètes.
   Il semble que les fichiers soient ingérés dans la base sans vérification technique ni harmonisation.
   
   Les professionnels de santé sont souvent mal identifiés par leur [n° RPPS](https://www.ameli.fr/medecin/exercice-liberal/vie-cabinet/rpps/rpps), et donc introuvables.
   
   Les organisations sont encore plus mal identifiées par leur n° SIRET/SIREN. 
   Sanofi, premier déclarant dans Transparence-Santé ne les renseigne jamais !

   Cette médiocrité des données brouille les pistes à tous les niveaux. 

1. Les déclarations ne sont **pas contrôlées**, donc douteuses.
 
   En cas de déclaration irrégulière, une entreprise [encourt 45 000 € d'amende](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000025053642).
   Cette somme est négligeable au vu des dépenses engagées. 
   L'écriture de la loi rend cette condamnation impossible en pratique, 
   car il faudrait démontrer que l'erreur est volontaire (*"… omettre **sciemment** de rendre publics…"*).
   
   Il faudrait que ces déclarations soient certifiées par des organismes public ou d'audit indépendants, ou a grand minima contrôlées par un organisme ayant un réel pouvoir de sanction, par exemple l'interdiction de réaliser les activités mal déclarées. 

1. De nombreuses **informations sont tenues secrètes**.

   Le décret d'application de la loi Bertrand avait largement vidé la loi de sa substance (voir la note _Article du CNOM sur le décret_) :
   - les rémunérations ne sont pas rendues publiques ;
   - les bénéficiaires finaux ne sont pas indiqués ;
   - l'objet des contrats est tenu secret.
   
  Suite à un recours de l'Ordre des médecins, le Conseil d'État a en partie [annulé](https://www.legifrance.gouv.fr/affichJuriAdmin.do?idTexte=CETATEXT000030283091) ce décret. 
  Cette décision a été complétée par un [décret fin 2016](https://www.legifrance.gouv.fr/eli/decret/2016/12/28/AFSX1637582D/jo/texte) et une [ordonnance début 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033893406&categorieLien=id).
  
  Dorénavant, les industriels doivent publier le montant des rémunérations, et identifier les bénéficiaires finaux des avantages.
  
  Cependant de nombreux contrats n'ont toujours pas de montant déclaré. 
  Le format des déclaration ne permet pas de déclarer les bénéficiaires finaux, et les industriels utilisent cette faille pour brouiller les pistes avec des organisations intermédiaires écrans (cf section suivante).
  
  L'objet des contrats reste lui toujours protégé par la loi, ce qui renforce les suspicions de corruption au lieu de rétablir la confiance.

<div class="ts-notes"> <strong>Notes :</strong>
    <ul>
        <li>
            <strong>Citation du LEEM </strong>
            (syndicat de l'industrie pharmaceutique), en <a href="http://www.vrai-faux.leem.org/post/167273340191/publi%C3%A9-dans-le-canard-encha%C3%AEn%C3%A9-le-8-novembre-2017">réponse</a> au <a href="https://www.slideshare.net/Market_iT/le-canard-enchain-20171108-des-labos-soignant-lthique"><i>Canard enchaîné</i></a> : "<strong>Oui, la lisibilité de la base transparence mériterait d’être améliorée.</strong>  La limite de la transparence aujourd’hui réside dans la complexité du dispositif mis en place, qui le rend difficilement lisible par le public. Des chiffres sont publiés sur la base transparence, mais les outils nécessaires pour les comprendre sont insuffisants voire inexistants. De la pédagogie mais également des améliorations du site sont aujourd’hui indispensables pour permettre à la transparence d’atteindre pleinement ses objectifs."  
        </li> 
        <li>
            <strong>Article du CNOM sur le décret :</strong>
            Article du CNOM du 23.05.2013, "Décret sur la publication des liens d’intérêt et la transparence : nous sommes très loin du compte" (<a href="https://www.conseil-national.medecin.fr/publications/communiques-presse/decret-dapplication-controler-declarations-dinteret">lien</a>)
        </li>
    </ul>
</div>


## Des organes de contrôle impuissants 

La DGCCRF a indépendamment lancé une série d'enquêtes, qui n'ont quasiment jamais abouties.
Ces enquêtes ont notamment été entravées par des [écrans placés entre les industriels et les bénéficiaires finaux](https://www.ccomptes.fr/sites/default/files/EzPublish/20160323-prevention-conflits-interets-en-matiere-expertise-sanitaire.pdf#page=32), selon trois stratégies : 

- des subventions sont versées à des associations de médecins ;
- les événements à destination des médecins sont organisés par des entreprises prestataires ;
- des filiales étrangères prennent en charge certaines dépenses lors des congrès.

Avant de signer certaines conventions, les industriels doivent les déclarer aux ordres professionnels pour avis. 
Cet avis n'avait pas de pas de pouvoir contraignant jusqu'au 1er octobre 2020. 
Depuis cette date, au-delà de certains montants, les conventions doivent autorisées, c'est-à dire l'avis devient contraignant.   

En lisant le rapport de la [Cour des comptes de mars 2016](https://www.ccomptes.fr/sites/default/files/EzPublish/20160323-prevention-conflits-interets-en-matiere-expertise-sanitaire.pdf#page=62), on apprend cependant que :
- les avis du CNOM concernant les conventions d'honoraires étaient massivement négatifs en 2015 (71%) ;    
- certains industriels refusent de soumettre des contrats à l'avis de l'ordre.

On attend donc de voir l'évaluation du nouveau dispositif mis en place, pour l'instant de façon assez opaque.
Une explication est disponible sur le site de l'[ordre des pharmaciens](http://www.ordre.pharmacien.fr/Nos-missions/Assurer-la-defense-de-l-honneur-et-de-l-independance/Dispositif-anti-cadeaux), 
mais la [plateforme officielle](https://eps.sante.gouv.fr/) ne fournit aucune information avant d'avoir pu créer un compte. 

Cette [interview](https://web.archive.org/web/20160530032245/https://www.macsf-exerciceprofessionnel.fr/Responsabilite/Humanisme-deontologie/conflits-interets-professionnels-de-sante-laboratoires) datant de 2016 du président du Conseil National de l’Ordre des Médecins reste un bon résumé de la situation.

`;


function ContextPage() {
    return (
        <ContentPage title="Contexte sur la base Transparence-Santé" content={pageText} path="/context"></ContentPage>
    );
}

export default ContextPage;
