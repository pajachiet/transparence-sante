import React from "react";
import EmbeddedMetabaseDashboard from "../../components/embedded_metabase_dashboard";
import {makeIframeUrl} from "../api/embed";
import Layout from "../../components/layout";
import {useRouter} from "next/router";
import {parseSlug} from "../../helpers/dashboard_data";
import {GetServerSideProps} from "next";


interface DashboardProps {
    initialIframeUrl: string;
}

function Slug({initialIframeUrl}: DashboardProps) {
    const router = useRouter();
    const slug = parseSlug(router.query.slug);

    if (!slug) {
        throw new Error('invalid dashboard slug: ' + slug);
    }

    return (
        <Layout>
            <EmbeddedMetabaseDashboard slug={slug} initialIframeUrl={initialIframeUrl}/>
        </Layout>
    );
}


export const getServerSideProps: GetServerSideProps = async context => {
    const slug = parseSlug(context.query.slug);

    if (!slug) {
        throw new Error('invalid dashboard slug: ' + slug);
    }

    const iframeUrl = makeIframeUrl(slug);
    console.log('==> serverSideProps in slug.tsx:', slug, iframeUrl);

    return {
        props: {
            initialIframeUrl: iframeUrl
        }
    }
}

export default Slug;


