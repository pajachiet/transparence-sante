import React from "react";
import ContentPage from "../components/content_page";

const pageText = `
<p style="text-align:center;">
<img 
    src="/images/base_transparence_sante.png" 
    alt="Base Transparence Santé" 
    width="600"
/>
</p>

## Données sources

Plusieurs sources de données sont téléchargées et exploitées.

#### Transparence Santé

Les entreprises déclarent chaque semestre les conventions, avantages et rémunérations :  
- Les déclarations correspondant au 1er semestre doivent être transmises au plus tard le 1er septembre.
- Les déclarations correspondant au 2ème semestre doivent être transmises au plus tard le 1er mars de l'année suivante. 
 
À noter que le système permet aussi de déclarer et de faire des corrections au fil de l'eau.

Les déclarations sont ensuite traitées et vérifiées, avant d'être intégrées dans Transparence Santé.
Certaines déclarations sont rejetées en erreur, et doivent être corrigées par l'industriel (ces vérifications sont trop [laxistes](/context#les-probl-mes-de-transparence-sant-)).
In fine, toutes les déclarations doivent être mises en ligne au plus tard le 1er octobre pour le premier semestre, et 1er avril de l'année suivante pour le second semestre.

Chaque nuit, une archive reprenant les données de la base est publiée sur le site [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/transparence-sante-1/).

Cette archive comprend 4 tables de données au format csv :

- un annuaire des **entreprises** ayant effectué une déclaration
- 3 fichiers de déclarations : 
    - **conventions** qui liste les contrats
    - **rémunérations** versées en contrepartie d'une prestation
    - **avantages** qui liste les cadeaux offerts sans contrepartie 


#### Association des entreprises à des groupes

Certaines filiales d'un même groupe déclarent séparément dans Transparence-Santé, ce qui complexifie les analyses.

Nous utilisons un [tableur collaboratif](https://docs.google.com/spreadsheets/d/1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E/edit#gid=1798640302) 
pour indiquer les [regroupements de filiales au sein d'un groupe](https://docs.google.com/spreadsheets/d/1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E/edit#gid=546061680). 

#### Annuaire santé 

L'[annuaire santé](https://annuaire.sante.fr/) (RPPS) est également téléchargé et nettoyé (essentiellement pour supprimer les doublons).

Il permet d'obtenir des informations fiables sur les professionnels bénéficiaires.
Il n'est pas utilisé par EurosForDocs pour nettoyer la base Transparence Santé, bien que cela fut un objectif par le passé.

## Nettoyage des données

Chaque nuit, EurosForDocs réalise un nettoyage des données.

Le code informatique utilisé est disponible sur <a href="https://gitlab.com/eurosfordocs/transparence-sante">Gitlab</a>.
Nous en détaillons les principales fonctions ci-dessous.

#### Un identifiant unique par déclaration

Un identifiant unique est créé pour chaque déclaration. 
Cet identifiant combine la catégorie de déclaration, l'identifiant de l'entreprise déclarante et l'identifiant de ligne de déclaration.

Cet identifiant est utilisé pour les traitements ultérieurs, et pour supprimer les quelques déclarations en doublon dans la base Transparence Santé.

#### Rattachement des avantages et rémunération aux convention 

Les déclarations de rémunérations et d'avantages ont un champ permettant d'indiquer le numéro d'une convention à laquelle elles sont liées.

Le remplissage de ce champ est obligatoire pour les rémunérations, et facultatif pour les avantages. Ce qui correspond au fait que toute rémunération doit se faire dans le cadre d'un contrat, tandis qu'un avantage peut être offert sans cadre contractuel.

Une convention peut ainsi être associée à 0, 1 ou plusieurs rémunérations et avantages. 

Par exemple, si un médecin est payé pour présenter à un congrès, l'industriel pourra déclarer
- une convention ;
- une rémunération ;
- un avantage pour le transport ;
- un ou plusieurs avantages pour l'hospitalité (nuit d'hôtel, repas) ;
- un avantage pour l'inscription au congrès.

###### Éviter les doubles comptes

Toute les rémunérations et avantages indiquent un montant. 
Les conventions **peuvent - ou non - indiquer un montant global**, a priori le total des rémunérations et avantages liés.

Ce point est une **difficulté majeure dans l'utilisation de la base Transparence-Santé**, qui empêche _a priori_ de sommer les montants des conventions avec ceux des rémunérations et avantages, au **risque de compter certains montants deux fois**.  

Pour **résoudre ce problème** et faciliter l'usage des données, EurosForDocs met rattache les rémunérations et avantages avec la convention liée
- 4 nouvelles colonnes indiquent pour chaque convention le nombre et le montant des rémunérations et avantage liés.
- La colonne principale de **<code>montant</code> pour chaque convention est le montant déclaré pour la convention, moins le montant des avantages et rémunérations liés, avec un résultat minimum à zéro**. 

<p style="text-align:center;" class="alert alert-success">
    Ces transformations permettent de sommer les montants des déclarations toutes catégories confondues, sans double compte, à l'exception des erreurs indiquées plus bas.
</p>


Pour information lorsque l'on étudie une convention
- Le montant déclaré initialement dans chaque convention est sauvegardé dans une nouvelle colonne.
- Un montant total de la convention est calculé dans une nouvelle colonnne, comme le maximum entre le montant déclaré pour la convention, et la somme des montants des rémunérations et avantages liés  

###### Contrats sans montant 

A contrario, certaines conventions ont un montant déclaré nul (ou vide), sans que l'on ne retrouve de rémunération ni d'avantages liés.

On ne peut donc pas connaître le montant de ces conventions, alors qu'il est obligatoire depuis 2017 de déclarer les rémunérations liées aux conventions 
(cf fin de la partie sur la [qualité des données](/context#les-probl-mes-de-transparence-sant-)); 
et rien ne justifie qu'une entreprise déclare des conventions sans montant - fut-il prévisionnel.

Ces conventions sont indentifiées par la valeur <code>True</code> dans la colonne indicatrice <code>montant_masque</code>.
Cette colonne permet de calculer un nombre de contrat sans montant traçable.


###### Avantages mal rattachés à leur convention
 
Les avantages pointent souvent vers des conventions dont le numéro n'existe pas dans Transparence-Santé, 
car l'existence de ces conventions n'est pas vérifiée avant d'intégrer les avantages à la base.


<p class="ts-notes">
    D'après la <a href="https://www.entreprises-transparence.sante.gouv.fr/flow/info_faq.xhtml">FAQ du site internet</a> :
    
    <strong>62. A quoi correspond le champ « AVANT_CONVENTION_LIE » ?</strong> 
    
    Cette information doit permettre de retrouver facilement la convention liée. Ce champ n'est pas contrôlé, vous pouvez donc choisir ce qui semble le plus pertinent pour retrouver la convention.
</p>

Par conséquent
- **le montant de certains avantages est compté en double** lorsqu'il est déclaré aussi dans la convention et que le lien est mal renseigné 
- **une parties des conventions sans montant traçable a bien un avantage (donc un montant) déclaré en lien**, mais l'erreur de numéro ne permet pas d'identifier ce montant

À noter que ce problème technique n'existe (presque) pas pour les rémunérations, 
car l'existence des conventions liées est vérifiée de façon stricte avant d'intégrer les rémunérations à la base..

#### Regroupement des entreprises

Les filiales des entreprises d'un même groupe sont regroupées sous un même nom pour faciliter la recherche 
(cf [fichier d'association décrit dans les sources](data#association-des-entreprises---des-groupes)).

Une colonne <code>entreprise_émmetrice</code> est créée, avec le nom du groupe, ou le nom d'origine si elle n'a pas été regroupée.

#### Catégorisation des déclarations

La base Transparence Santé ne contient pas de catégorisation des déclarations, ce qui empêche de nombreuses analyses.

EurosForDocs cherche à recréer de telles catégories en s'appuyant sur la colonne qui donne la nature de l'avantage ou l'objet de la convention.

Ce travail a été réalisé pour les avantages, mais sans vraiment le finaliser et sans relecture qualitative.
Il n'a pas (encore) été réalisé sur les conventions, par manque de temps. Pour les rémunérations il suffira de s'appuyer sur le liens avec les conventions.

##### Nomenclature des catégories

La nomenclature de catégories utilisée s'appuie sur la nomenclature du [code de déclaration de l'EFPIA](https://www.efpia.eu/relationships-code/disclosure-of-payments/). 

Nous avons ajouté des catégories correspondants à des liens déclarés en France, mais exclu de ce code d'autorégulation (au rabais) de l'industrie.
Nous avons également ajouté des sous-catégories plus fines, en fonction de ce qui était observé dans les données.

Au final nous obtenons cet arbre de catégorie ([code source](https://gitlab.com/eurosfordocs/transparence-sante/-/blob/master/src/constants/ontology.py)), 
avec une étoile <code>*</code> devant les catégories existantes dans le code EFPIA. 
<pre>
    * Lien d'intérêt
    ├── * Dons et Subventions
    │   ├── Dons
    │   │   ├── Dons de sommes d'argent
    │   │   └── Don en nature
    │   └── Subventions
    ├── * Contribution au coût d'événements promotionnels, scientifique ou professionnel
    │   ├── * Mécénat
    │   ├── * Frais d'inscriptions
    │   └── * Transport et Hospitalité
    │       ├── Transport
    │       └── Hospitalité
    │           ├── Hébergement
    │           └── Restauration
    ├── * Service et Conseil
    │   ├── * Honoraires
    │   └── * Dépenses liées
    ├── * Recherche et Développement
    ├── Repas et Boissons en dehors d'un événement
    ├── Cadeaux
    ├── Formation
    └── Sans classe
        ├── Vide, Autre
        └── Association à une catégorie non réussie
</pre>

L'association des avantages aux catégories se fait en s'appuyant sur une [liste de mots clés](https://gitlab.com/eurosfordocs/transparence-sante/-/blob/master/src/specific/map_to_ontology.py), 
puis par des stratégies de distance textuelles.

Sont restitués 2 niveaux de catégories : 
- la catégorie la plus fine ainsi identifiée,
- la catégorie parente (ou ancètre) la plus large (1er niveau dans l'arbre). 

#### Nettoyages de forme

D'autres petits nettoyages de forme sont réalisés
- Les noms et prénoms des professionnels bénéficiaires sont harmonisés
    - suppression des caractères spéciaux, dont tirets, accents et espaces inutiles
    - harmonisation de la casse : nom en majuscule, prénom avec la première lettre de chaque mot en majuscule
    - création d'un champ combiné "NOM Prénom"
- Les noms des structures bénéficiaires sont harmonisés de façon similaire
- Les dates invalides ou aberrantes sont corrigées
- Le détail des avantages et convention est harmonisé : suppression des caractères spéciaux, harmonisation des valeurs manquantes
- Des champs combinés sont créés pour 
    - les adresses
    - le détail des conventions, auquel on ajoute le détail des événéments liés
- De nombreuses colonnes sont renommées, et uniformisées entre les conventions, avantages et rémunérations, de façon à pouvoir combiner toutes les déclarations en une table.


<div class="ts-notes">
    <strong>Note :</strong> Les identifiants des bénéficiaires ne sont pas corrigés, ni l'orthographe et les variants d'écriture des noms.    
</div>


## Fichiers nettoyés

Les fichiers nettoyés sont téléchargeables en suivant les liens suivants :

- [entreprise.csv.gz](https://eurosfordocs.fr/download/dump/entreprise.csv.gz)
- déclarations
    - [declaration_avantage.csv.gz](https://eurosfordocs.fr/download/dump/declaration_avantage.csv.gz)
    - [declaration_convention.csv.gz](https://eurosfordocs.fr/download/dump/declaration_convention.csv.gz)
    - [declaration_remuneration.csv.gz](https://eurosfordocs.fr/download/dump/declaration_remuneration.csv.gz)
- Annuaire santé, divisé en 3 tables
    - [professionnel_sante.csv.gz](https://eurosfordocs.fr/download/dump/professionnel_sante.csv.gz)
    - [etablissement_sante.csv.gz](https://eurosfordocs.fr/download/dump/etablissement_sante.csv.gz)
    - [activite_professionnel_sante.csv.gz](https://eurosfordocs.fr/download/dump/activite_professionnel_sante.csv.gz) (lien entre les tables précédentes)

Ils sont soumis aux mêmes restrictions d'usage que les fichiers sources (voir la page [d'avertissements](/warning)).



<p class="alert alert-danger">
    <li>Ne pas hésiter à poser des questions si vous utilisez ces données.</li>
    <li>Le format de ces fichiers est amené à changer sans avertissement.</li>  
</p>

## Tables PostgreSQL 

Chacun de ces fichiers est nettoyé, puis ingéré dans une table d'une PostgreSQL, respectivement appelées :
- **entreprise** 
- **declaration_convention**
- **declaration_avantage** 
- **declaration_remuneration**
- **annuaire**

Une vue **declaration** reprend les colonnes communes des 3 tables de déclarations, pour simplifier des analyses communes.
Cette vue est la base des tableaux de bord exposés sur Metabase.
`;


function DataPage() {
    return (
        <ContentPage title="Traitement des données" content={pageText} path="/data"></ContentPage>
    );
}

export default DataPage;
