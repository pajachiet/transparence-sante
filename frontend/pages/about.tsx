import React from "react";
import ContentPage from "../components/content_page";

const pageText = `

## L'association
EUROS FOR DOCS est une association loi 1901.

Nous croyons en une médecine au service des patients, indépendante d’intérêts économiques privés.

Notre objet social est de _mener à bien des projets liés à la transparence et à l'indépendance de l'expertise, notamment dans les domaines de la santé, de l'environnement, et de l'alimentation_. 

Après 2 années d'existence sous la forme d'un collectif, les [statuts d'association](https://www.journal-officiel.gouv.fr/associations/detail-annonce/associations_b/20200026/1371) ont été déposés en juin 2020.

### Bureau

- Président : Luc Martinon, Data Scientist, principal développeur du projet européen eurosfordocs.eu
- Trésorier : Adrien Chauve, Développeur

### Autres membres

- Pierre-Alain Jachiet : Fondateur du projet, et principal développeur du projet français eurosfordocs.fr

D'autres membres participent régulièrement à des projets spécifiques. 
Nous [cherchons toujours des compétences variées](/contributing) pour qui souhaiterait s'impliquer activement.

EurosForDocs a été accompagné durant les saisons 5 et 6 de l'association [DataForGood](https://dataforgood.fr), 
pour des projets portant respectivement sur l'[amélioration du nettoyage](https://dataforgood-fr.medium.com/et-si-la-technologie-permettait-plus-de-transparence-dans-linfluence-de-l-industrie-ba001517ab47) 
et sur le [croisement avec la base DPI santé](https://docs.google.com/presentation/d/1Wbdg6SSrG4_dzjLPCVSwFFgWV4BenOb0qGEqQtWuUcw/edit#slide=id.g4ee2b9c23b_0_0). 

La taille de l'équipe s'est agrandie à une dizaine de contributeurs durant ces saisons. 

EurosForDocs a également initié son projet européen via le renfort d'étudiants dans le cadre de l'association [Latitudes](https://www.latitudes.cc/).

## Réseau

<i class="fab fa-connectdevelop fa-fw" style="color:DarkBlue"></i> EurosForDocs offre des outils pour simplifier le travail d'autres acteurs.

Nous sommes ainsi en relation directe avec divers associations, chercheurs, journalistes et institutions.

## Un projet libre 

<i class="fab fa-osi fa-fw"></i> 
EurosForDocs est un projet libre.
Le [code source](https://gitlab.com/eurosfordocs/transparence-sante) est publié sous _licence GNU AGPLv3_, qui garantit à quiconque la liberté de le réutiliser. 

La liste des contributeurs au code source est [disponible sur cette page](https://gitlab.com/eurosfordocs/transparence-sante/-/graphs/master).


<div class="ts-notes">
Vous avez le droit de réutiliser, étudier, dupliquer et modifier le code source du projet. La caractéristique <a href="https://fr.wikipedia.org/wiki/Copyleft">copyleft</a> de la licence impose cependant que toute évolution du code doit être publiée sous les mêmes conditions, sans restrictions de droits.
</div>


`;


function Page() {
    return (
        <ContentPage title="Qui sommes nous ?" content={pageText} withToc={true} path="/about"></ContentPage>
    );
}

export default Page;
