import React from "react";
import ContentPage from "../components/content_page";

const pageText = `
Des **tableaux simplifiés** sont accessibles sans connexion pour chercher dans EurosForDocs, après avoir lu les [avertissements](/explore#restriction-d-usage).
 

<p style="text-align:center;">
     <a class="btn btn-success btn-large" href="/dashboard/entreprise_declarante" style="padding:15px 30px; margin:10px;">
    Recherche par entreprise déclarante
    </a>
    <a class="btn btn-success btn-large" href="/dashboard/professionnel_beneficiaire" style="padding:15px 30px; margin:10px;">
    Recherche par professionnel bénéficiaire
    </a>
    <a class="btn btn-success btn-large" href="/dashboard/structure_beneficiaire" style="padding:15px 30px; margin:10px;">
    Recherche par structure bénéficiaire
    </a>

</p>


Un **annuaire des professionels de santé** est également accessible sans connexion, permettant de retrouver leur numéro RPPS. 
<p style="text-align:center;">
    <a class="btn btn-success btn-large" href="/dashboard/annuaire_professionnels" style="padding:15px 30px; margin:10px;">
    Annuaire des professionnels de santé
    </a>
</p>

Les **fonctionnalités avancées** nécessitent de se connecter à la plateforme.

<p style="text-align:center;">
    <a class="btn btn-success btn-large" href="/explore" style="padding:15px 30px; margin:10px;">
    Fonctionnalités avancées
    </a>
</p>

`;


function DashboardPage() {
    return (
        <ContentPage title="Accès rapide" content={pageText} path="/dashboard"></ContentPage>
    );
}

export default DashboardPage;
