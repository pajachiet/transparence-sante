import React from "react";
import ContentPage from "../components/content_page";

const pageText = `

## Usages autorisés

En utilisant EurosForDocs, vous devenez _réutilisateur_ de la base Transparence-Santé, et devez respectez les restrictions légales de son utilisation (cf [licence](/download/Licence_reutilisation_donnees_transparence_sante.pdf)). 

En particulier, tout usage : 
- doit avoir pour finalité la transparence des liens d’intérêts,
- ne peut se faire à titre strictement commercial,
- doit citer la source des données.


## Limitations de la recherche

La recherche dans EurosForDocs a plusieurs **limitations**

- La recherche se fait par **complétion stricte** des noms. On ne peut pas chercher en inversant les termes ou en commençant au milieu.

- **Certains professionnels peuvent avoir des homonymes**. Pour se restreindre à un professionnel unique, il faut donc filtrer selon son [identifiant RPPS](https://www.ameli.fr/medecin/exercice-liberal/vie-cabinet/rpps/rpps). On perd alors les déclarations concernant ce professionnel, pour lesquelles l'identifiant n'est pas renseigné.

- Ne pas utiliser d'accent ou de caractère spécial, nous les avons supprimé pour limiter la variabilité des orthographes.

- Les **copier-coller depuis une liste ne fonctionneront généralement pas**, sauf à harmoniser les noms de façon exactement identique, majuscules et minuscules compris.


## Erreurs dans les déclarations

<p style="text-align:center;" class="alert alert-danger">
    <strong>Attention !</strong>
    La base Transparence-Santé contient des déclarations 
    <a href="https://abonnes.lemonde.fr/les-decodeurs/article/2017/10/12/les-rates-de-la-base-de-donnees-publique-transparence-sante_5199937_4355770.html">erronées</a>. 
</p>

L'exactitude des contenus publiés est de la responsabilité des entreprises ayant procédé aux déclarations. 

EurosForDocs ne peut être tenu responsable d'informations inexactes ou incomplètes issues de cette base.

En particulier, EurosForDocs ne devine pas les identifiants manquants, et ne corrige pas les variations et erreurs dans les noms.

En cas d'erreur, les benéficiaires peuvent faire valoir leur droit de _rectification_ 
- sur le [site gouvernemental](https://www.transparence.sante.gouv.fr/) pour les personnes physiques (explication sur l'onglet Mode d'emploi),
- en s'adressant à l'entreprise pour les personnes morales.

Ces rectifications seront alors prises en compte sur EurosForDocs.

## Nettoyage par EurosForDocs 

Pour simplifier l'accès à la base Transparence-Santé, 
les données affichées dans EurosForDocs font l'objet d'un [nettoyage](/data#nettoyage-des-donn-es) quotidien. 

Cependant : 
- le nettoyage est incomplet ;
- des erreurs de traitement sont possibles ; 
- malgré nos efforts de clarification, des erreurs d'interprétation sont vite arrivées. 

<p style="text-align:center;" class="alert alert-danger">
    Pour toutes ces raisons, il est recommandé de <strong>demander conseil</strong> avant toute publication utilisant EurosForDocs.
</p>

`;


function ContextPage() {
    return (
        <ContentPage title="Avertissements" content={pageText} path="/warning"></ContentPage>
    );
}

export default ContextPage;
