# Frontend for EurosForDocs

This version replace the previous Jekyll version. 

It allows to use a backend to integrate public Metabase dashboards.

# Usage

## Development

- To install the dependencies:

        yarn # or npm install if you prefer
    
- To start the development server:
    
        yarn dev
        
- Copy the .env-template to your own .env non-versionned file and set the values:

        cp .env-template .env

Note : the secret key will be found at https://www.eurosfordocs.fr/metabase/admin/settings/embedding_in_other_applications

## Production


- Build the app (with some pages statically prerendered)

        yarn build
        
- Start Nextjs server:

        yarn start

- When using the frontend with the docker-compose, it will be easier the take `.env` values from the global project `.env` file 