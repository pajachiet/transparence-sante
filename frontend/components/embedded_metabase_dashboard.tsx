import React, {useState} from "react";
import AsyncSelect from "react-select/async";
import Head from "next/head";
import {ValueType} from "react-select";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import {DashboardConfig, dashboardConfigs, DashboardSlug, Filter} from "../helpers/dashboard_data";
import Seo from "./seo";
import Container from "react-bootstrap/Container";

type SelectOption = { label: string, value: string };

async function loadOptions(dashboardConfig: DashboardConfig | undefined, filter: Filter, prefix: string): Promise<SelectOption[]> {
    if (!prefix || !dashboardConfig) {
        return [];
    }

    const response = await fetch('/api/load_filter_data',
        {
            method: 'POST',
            body: JSON.stringify({
                slug: dashboardConfig.slug,
                fieldId: filter.fieldId,
                prefix: prefix,
            })
        });
    const result = await response.json();
    return result.map((value: string) => {
        return {
            value: value,
            label: value
        };
    });
}

async function getIframeUrl(config: DashboardConfig, enterprise: string | null): Promise<string> {
    const response = await fetch('/api/embed', {
        method: 'POST',
        body: JSON.stringify({
            slug: config.slug,
            filterValues: [enterprise]
        })
    });
    const result = await response.json();
    return result.iframeUrl;
}


interface EmbeddedMetabaseDashboardProps {
    slug: DashboardSlug;
    initialIframeUrl: string;
}

const EmbeddedMetabaseDashboard: React.FunctionComponent<EmbeddedMetabaseDashboardProps> = ({slug, initialIframeUrl}) => {
    const [localIframeUrl, setLocalIframeUrl] = useState(initialIframeUrl);

    const dashboardConfig = dashboardConfigs.get(slug);
    if (dashboardConfig === undefined) {
        throw new Error('invalid dashboard slug: ' + slug);
    }

    function _createIframeCode() {
        return {
            __html: `
            <iframe
                src="${localIframeUrl}"
                frameborder="0"
                width="100%"
                scrolling="no"
                allowtransparency
                onload="iFrameResize({heightCalculationMethod : 'max'}, this)"
                style="height: 2000px"
            ></iframe>
        `
        };
    }

    async function updateIframeUrl(selectedEnterprise: SelectOption): Promise<void> {
        const iframeUrl = await getIframeUrl(dashboardConfig!, selectedEnterprise ? selectedEnterprise.value : null);
        setLocalIframeUrl(iframeUrl);
    }

    function renderFilter(filter: Filter) {
        return (
            <AsyncSelect cachedOptions
                         defaultOptions
                         isClearable
                         onChange={(selectedOption: ValueType<SelectOption>) => {
                             updateIframeUrl(selectedOption as SelectOption);
                         }}
                         loadOptions={prefix => loadOptions(dashboardConfig, filter, prefix)}
                         noOptionsMessage={() => `Tapez le début d'${filter.label}...`}
                         loadingMessage={() => "Chargement..."}
                         placeholder={`Sélectionner ${filter.label}...`}
                         instanceId={`select_${filter.fieldName}`}/>
        )
    }

    return (
        <div>
            <Seo title={dashboardConfig.title} path={"/dashboard/" + dashboardConfig.slug}></Seo>
            <Head>
                <script src="https://eurosfordocs.fr/metabase/app/iframeResizer.js"/>
            </Head>
            <Container>
                <Row style={{padding: 30}}>
                    <Col sm={9}>
                        <h1>
                            {dashboardConfig.title}
                        </h1>
                    </Col>
                    <Col sm={3}>
                        {dashboardConfig.filters.map(filter =>
                            renderFilter(filter))
                        }
                    </Col>
                </Row>

                <div className="row">
                    <div className="col-12">
                        <div dangerouslySetInnerHTML={_createIframeCode()}/>
                    </div>
                </div>
            </Container>

            <style jsx>{`
                h1 {
                    font-size: 2rem;
                }
            `}</style>
        </div>
    );
};


export default EmbeddedMetabaseDashboard;

