import Header from './header';
import React, {FunctionComponent} from "react";
import Footer from "./footer";

interface Props {
}

const Layout: FunctionComponent<Props> = props => (
    <div>
        <Header/>
        {props.children}
        <Footer/>
    </div>
);

export default Layout;
