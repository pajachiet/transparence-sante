import React from "react";
import Container from "react-bootstrap/Container";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope, faEuroSign} from "@fortawesome/free-solid-svg-icons";
import {faGitlab, faTwitter} from '@fortawesome/free-brands-svg-icons'


const Footer = () => {
    return (
        <div>
            <footer className="ts-footer">
                <Container>
                    <ul className="ts-social-icons">
                        <li><strong>Contact</strong></li>

                        <li>
                            <a href="mailto:contact@eurosfordocs.fr">
                                <FontAwesomeIcon icon={faEnvelope}/> email
                            </a>
                        </li>

                        <li>
                            <a href="https://gitlab.com/eurosfordocs/transparence-sante">
                                <FontAwesomeIcon icon={faGitlab}/> gitlab
                            </a>
                        </li>

                        <li>
                            <a href="https://opencollective.com/eurosfordocs">
                                <FontAwesomeIcon icon={faEuroSign}/> opencollective
                            </a>
                        </li>

                        <li>
                            <a href="https://twitter.com/eurosfordocs">
                                <FontAwesomeIcon icon={faTwitter}/> twitter
                            </a>
                        </li>
                    </ul>

                    <div>© 2020 EurosForDocs.fr.
                    </div>
                </Container>
            </footer>

            <style jsx>{`
                .ts-footer {
                    background-color: #0092ca;
                    color: white;
                    padding: 50px 0;
                    margin-top: 50px;
                }
                
                .ts-footer ul {
                    list-style-type: none;
                    padding-left: 0;
                }
                
                .ts-footer ul li {
                    display: inline;
                    padding-right: 20px;
                    color: white;
                }
                
                .ts-footer a {
                    color: white;
                    display: inline-block;
                }
                
                .ts-social-icons {
                    text-transform: uppercase;
                    font-weight: bold;
                }
              
            `}</style>
        </div>
    )
};

export default Footer;
