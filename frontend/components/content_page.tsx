import React from "react";
import CustomMarkdown from "../components/custom_markdown";
import Layout from "../components/layout";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Seo from "./seo";

interface ContentPageProps {
    title: string;
    path: string;
    content: string;
    withToc?: boolean;
}

function ContentPage(props: ContentPageProps) {
    const useTocForPageContent = props.withToc === undefined ? true : props.withToc;

    function _renderContent() {
        const renderedContent = <CustomMarkdown source={props.content} withToc={useTocForPageContent}/>;
        if (useTocForPageContent) {
            return renderedContent;
        }
        return (
            <Col md={{span: 6}}>
                {renderedContent}
            </Col>
        )
    }

    return (
        <>
            <Seo title={props.title} path={props.path}></Seo>
            <Layout>
                <Container>
                    <Row>
                        <Col md={{offset: 3, span: 9}}>
                            <h1 className="ts-header">{props.title}</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={{span: 3}} className="d-none d-md-block">
                        </Col>
                        {_renderContent()}
                    </Row>
                </Container>
            </Layout>
        </>
    );
}

export default ContentPage;
