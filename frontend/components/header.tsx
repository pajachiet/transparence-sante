import React, {FunctionComponent} from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Container from "react-bootstrap/Container";
import Link from "next/link";

interface Props {
    isHome?: boolean;
}

const Header: FunctionComponent<Props> = () => {
    return (
        <div>
            <Navbar bg="light" expand="lg" sticky="top" className="ts-navbar">
                <Container>
                    <Navbar.Brand>
                        <Link href="/">
                            <img src="/images/efd_logo_64.png"
                                 alt="Logo EurosForDocs"
                            />
                        </Link>
                        <Link href="/">
                            <a>Euros for Docs</a>
                        </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                        </Nav>
                        <Nav>
                            <Nav.Link href="/">Accueil</Nav.Link>
                            <Nav.Link href="/explore">Connexion</Nav.Link>
                            <NavDropdown title="Explications" id="basic-nav-dropdown">
                                <NavDropdown.Item href="/warning">Avertissements</NavDropdown.Item>
                                <NavDropdown.Item href="/context">Contexte sur Transparence-Santé</NavDropdown.Item>
                                <NavDropdown.Item href="/presentation">Objectifs de EurosForDocs</NavDropdown.Item>
                                <NavDropdown.Item href="/data">Traitement des données</NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="À propos" id="basic-nav-dropdown">
                                <NavDropdown.Item href="/about">Qui sommes nous ?</NavDropdown.Item >
                                <NavDropdown.Item  href="/contributing">Nous soutenir</NavDropdown.Item >
                                <NavDropdown.Item href="/legal">Données personnelles</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

            <style jsx>{`
                :global(.ts-navbar) {
                    padding-top: 15px;
                    padding-bottom: 15px;
                } 
                
                :global(.ts-navbar) a {
                    color: inherit;
                    text-decoration: inherit;
                } 
            `}</style>
        </div>
    )
};

export default Header;
