import React, {FunctionComponent} from "react";
import Head from "next/head";

interface Props {
    title: string;
    path?: string;
}

const Seo: FunctionComponent<Props> = ({title, path}) => {
    const url = "https://www.eurosfordocs.fr" + (path || "");
    console.log(title, path, url);
    return (
        <Head>
            <title>{title} | Euros For Docs</title>
            <meta name="description" content="Pour la transparence du lobbying des industries de santé."/>

            <meta property="og:type" content="website"/>
            <meta property="og:locale" content="fr_FR"/>
            <meta property="og:site_name" content="Euros For Docs"/>
            <meta property="og:title" content={title}/>
            <meta property="og:url" content={url}/>
            <meta property="og:description" content="Pour la transparence du lobbying des industries de santé."/>
            <meta property="og:image"
                  content="https://www.eurosfordocs.fr/assets/images/trust-transparency.jpg?maxwidth=573"/>

            <meta name="twitter:site" content="@eurosfordocs"/>
            <meta name="twitter:title" content={title}/>
            <meta name="twitter:description" content="Pour la transparence du lobbying des industries de santé."/>
            <meta name="twitter:url" content={url}/>
            <meta name="twitter:card" content="summary_large_image"/>
            <meta name="twitter:image"
                  content="https://www.eurosfordocs.fr/assets/images/trust-transparency.jpg?maxwidth=573"/>

            <script
                type='application/ld+json'
                dangerouslySetInnerHTML={{
                    __html: JSON.stringify({
                        "@context": "http://schema.org",
                        "@type": "Person",
                        "name": "EurosForDocs.fr",
                        "url": "https://www.eurosfordocs.fr",
                        "sameAs": null
                    })
                }}
            />
        </Head>
    )
};

export default Seo;
