# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
# ---

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 30
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100

ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import CLEANED_DATA_DIR, RAW_DATA_DIR
import src.constants.csv as csv

# +
DATA_DIR = RAW_DATA_DIR
cleaned_remuneration_path = os.path.join(DATA_DIR, csv.REMUNERATION_CSV)
#df_remuneration = pd.read_csv(cleaned_remuneration_path, sep=csv.DEFAULT_SEPARATOR)

cleaned_convention_path = os.path.join(DATA_DIR, csv.CONVENTION_CSV)
df_convention = pd.read_csv(cleaned_convention_path, sep=csv.DEFAULT_SEPARATOR)

cleaned_avantage_path = os.path.join(DATA_DIR, csv.ADVANTAGE_CSV)
#df_avantage = pd.read_csv(cleaned_avantage_path, sep=csv.DEFAULT_SEPARATOR)
# -

df_convention.conv_objet_autre.value_counts()

print(df_remuneration.shape)
df_remuneration.sample(2)

print(df_convention.shape)
df_convention.sample(2)

print(df_avantage.shape)
df_avantage.sample(2)



