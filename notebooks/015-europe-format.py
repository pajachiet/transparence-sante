# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import os

os.chdir('..')

from src.constants.directories import DUMP_DATA_DIR
from os.path import join as p_join

# +
import zipfile
import shutil
import requests
import pandas as pd
import numpy as np
from src.utils import *

pd.options.display.max_columns = 50
pd.options.display.max_rows = 50
pd.options.display.max_colwidth = 300
# -

nrows = 10000

# # Common variables

# +
dump_dir = DUMP_DATA_DIR
sunshine_europe_dump_dir = p_join(DUMP_DATA_DIR, "sunshine_europe")

os.makedirs(sunshine_europe_dump_dir, exist_ok=True)
# -

eurosfordocs_files = [
    "entreprise.csv.gz",
    "declaration_avantage.csv.gz",
    "declaration_convention.csv.gz",
    "declaration_remuneration.csv.gz",
    "professionnel_sante.csv.gz",
    "etablissement_sante.csv.gz",
    "activite_professionnel_sante.csv.gz"
]

# +
link_columns = ['link_id', 'publication_id', 'source_organization_id',
                'recipient_entity_id', 'recipient_entity_type', 'related_activity_id',
                'type', 'category', 'details', 'date', 'year', 'value_total_amount',
                'value_total_amount_eur', 'currency', 'is_aggregated',
                'number_of_aggregated_recipients']

entity_columns = ['entity_id', 'directory_id', 'directory_entity_id', 'full_name',
                  'is_person', 'person_first_name', 'person_last_name',
                  'person_profession', 'person_specialty', 'person_organization_name',
                  'entity_type', 'organization_activity_area', 'website', 'location',
                  'address', 'city', 'state_province_region', 'postal_code', 'country',
                  'latitude', 'longitude', 'valid_since', 'valid_until']


# +
def select_and_rename_columns(df, columns_mapping):
    df = df[list(columns_mapping.keys())]
    return df.rename(columns=columns_mapping)


def create_columns_with_default_values(df, columns_default_values):
    for column, value in columns_default_values.items():
        df[column] = value
    return df


def align_to_columns_list(df, columns_list):
    # fill missing columns
    df = df.copy()
    for col in columns_list:
        if col not in df.columns:
            df[col] = ''
    return df[columns_list]


def to_csv_append(df, filepath):
    if not os.path.isfile(filepath):
        df.to_csv(filepath, index=False)
    else:
        df.to_csv(filepath, index=False, mode='a', header=False)


# -

# +
def download(url, file_path, reset=True):
    if not reset and os.path.exists(file_path):
        print(f"Target file exists {file_path} and not reset")
    else:
        r = requests.get(url, stream=True, verify=True)
        i = 0
        with open(file_path, 'wb+') as f:
            for chunk in r.iter_content(10 ** 5):
                i += 1
                f.write(chunk)


download("https://docs.google.com/spreadsheets/d/1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E/export?format=csv",
         p_join(dump_dir, "fr_mapping_subsidiaries_group.csv"))

download(
    "https://docs.google.com/spreadsheets/d/1TutKp_r3MCTgJXDHKKiZG7-ItdZ2OKqg0CFd-L4ek1E/export?format=csv&gid=546061680",
    p_join(dump_dir, "fr_manual_industry.csv"))

# -

# # Normalize

# ## Entities

# ### Companies

# #### Parent companies

df_manual_industry = pd.read_csv(p_join(dump_dir, 'fr_manual_industry.csv'), nrows=nrows).dropna()

# +
columns_mapping = {
    'groupe_entreprise': 'full_name',
    'identifiant_groupe': 'entity_id'
}
columns_default_values = {
    'directory_id': 'france_parent_companies',
    'is_person': False,
    'entity_type': 'Industry'
}

df = select_and_rename_columns(df_manual_industry, columns_mapping)
df['directory_entity_id'] = df['entity_id']
df = create_columns_with_default_values(df, columns_default_values)
df = align_to_columns_list(df, entity_columns)

df.to_csv(p_join(sunshine_europe_dump_dir, 'entity__parent_companies.csv'), index=False)
print(df.shape)
# -

# #### Disclosing companies

# +
columns_mapping = {
    'identifiant_entreprise': 'entity_id',
    'entreprise_émmetrice': 'clean_entity_id',
    'filiale_déclarante': 'full_name',
    'secteur': 'organization_activity_area',
    'adresse': 'address',
    'ville': 'city',
    'code_postal': 'postal_code',
    'code_pays': 'country'}

columns_default_values = {
    'directory_id': 'france_disclosing_companies',
    'is_person': False,
    'entity_type': 'Industry'
}

# -

manual_group_to_id = df_manual_industry.set_index('groupe_entreprise').to_dict()['identifiant_groupe']

# +
df_entreprise = pd.read_csv(p_join(dump_dir, 'entreprise.csv.gz'), sep=';', nrows=nrows)
df_entreprise["entreprise_émmetrice"] = df_entreprise['entreprise_émmetrice'].map(manual_group_to_id)

df = select_and_rename_columns(df_entreprise, columns_mapping)
df = create_columns_with_default_values(df, columns_default_values)
df['directory_entity_id'] = df['entity_id']

df = align_to_columns_list(df, entity_columns)

df.to_csv(p_join(sunshine_europe_dump_dir, 'entity__disclosing_companies.csv'), index=False)
print(df.shape)


# -
# ### Recipient

def get_df_entity_from_declaration(df_declaration):
    columns_mapping = {
        'beneficiaire_adresse': 'address',
        'beneficiaire_code_postal': 'postal_code',
        'beneficiaire_pays': 'country',
        'beneficiaire_ville': 'city',
        'catégorie_beneficiaire': 'entity_type',
        'declaration_id': 'entity_id',
        'identifiant': 'directory_entity_id',
        'type_identifiant': 'directory_id',
        'nom': 'person_last_name',
        'profession': 'person_profession',
        'prénom': 'person_first_name',
        'specialité': 'person_specialty',
        'nom_prénom': 'name_surname',
        'structure_bénéficiaire': 'recipient_organization',
    }

    entity_type_to_is_person = {
        'Professionnel de santé': True,
        'Etudiant': True,
        'Personnes morales assurant la formation initiale ou continue des professionnels de santé': False,
        "Association d'étudiants": False,
        'Association usager de santé': False,
        'Vétérinaire Personne Morale': False,
        'Vétérinaire': True,
        'Etablissement de santé': False,
        'Editeur de logiciel': False,
        'Groupement professionels agricoles': False,
        'Académies, Fondation, sociétés savantes, organismes de conseils': False,
        'Association professionnel de santé': False,
        'Presse et média': False,
        'Groupement sanitaire': False
    }

    df = select_and_rename_columns(df_declaration, columns_mapping)

    df['entity_id'] = 'fr_recipient_' + df['entity_id']
    df['is_person'] = df['entity_type'].map(entity_type_to_is_person)
    df['full_name'] = df['name_surname'].where(df['is_person'], df['recipient_organization'])
    df['person_organization_name'] = df['recipient_organization'].where(df['is_person'], np.nan)

    mask_type_id_autre = df['directory_id'] == 'AUTRE'
    df.loc[mask_type_id_autre, 'directory_id'] = ''
    df.loc[mask_type_id_autre, 'directory_entity_id'] = ''

    return align_to_columns_list(df, entity_columns)


def to_iso_date(s):
    "convert '28/06/2019' to '2019-06-28'"
    if type(s) == str:
        return s[6:12] + '-' + s[3:5] + '-' + s[0:2]
    else:
        return s


CONV_SEP = '|'


def prepare_df_convention(df_convention):
    event_pattern = "(Manifestation : '(?P<event>[^']*)')? ?" \
                    "(Organisé le : '(?P<event_date>[^']*)')? ?" \
                    "(À : '(?P<place>[^']*)')? ?" \
                    "(Par : '(?P<organizer>[^']*)')?"

    contract_pattern = "(Début contrat (?P<contract_start>[\d/]*))? ?" \
                       "(Fin contrat (?P<contract_end>[\d/]*))?"

    df_detail = df_convention['detail'].str.split(CONV_SEP, expand=True).apply(lambda x: x.str.strip())
    df_detail.columns = ["detail", "event", "contract"]

    df_convention["has_event"] = df_detail["event"].map(bool)

    df_convention = pd.concat([df_convention,
                               (df_detail["event"].str.extract(event_pattern)[
                                   ["event", "event_date", "place", "organizer"]]),
                               (df_detail["contract"].str.extract(contract_pattern)[
                                   ["contract_start", "contract_end"]])], axis=1)

    df_convention["event_date"] = df_convention["event_date"].map(to_iso_date)
    df_convention["contract_start"] = df_convention["contract_start"].map(to_iso_date)
    df_convention["contract_end"] = df_convention["contract_end"].map(to_iso_date)

    df_convention['detail'] = df_detail['detail']

    df_convention['related_activity_id'] = np.nan
    df_convention.loc[df_convention.has_event, 'related_activity_id'] = (
            'fr_activity_' + df_convention.loc[df_convention.has_event, 'declaration_id'])
    return df_convention


# %%time
outfile = p_join(sunshine_europe_dump_dir, 'entity__recipient_convention.csv')
if os.path.exists(outfile):
    os.remove(outfile)
for df in pd.read_csv(p_join(dump_dir, 'declaration_convention.csv.gz'),
                      sep=';', dtype=str, chunksize=10 ** 6, nrows=nrows):
    df = get_df_entity_from_declaration(df)
    to_csv_append(df, outfile)
    print(df.shape)

# +
# %%time

outfile = p_join(sunshine_europe_dump_dir, 'entity__recipient_remuneration.csv')
if os.path.exists(outfile):
    os.remove(outfile)
for df in pd.read_csv(p_join(dump_dir, 'declaration_remuneration.csv.gz'),
                      sep=';', dtype=str, chunksize=10 ** 6, nrows=nrows):
    df = get_df_entity_from_declaration(df)
    to_csv_append(df, outfile)
    print(df.shape)

# +
# %%time

outfile = p_join(sunshine_europe_dump_dir, 'entity__recipient_avantage.csv')
if os.path.exists(outfile):
    os.remove(outfile)
for df in pd.read_csv(p_join(dump_dir, 'declaration_avantage.csv.gz'),
                      sep=';', dtype=str, chunksize=10 ** 6, nrows=nrows):
    df = get_df_entity_from_declaration(df)
    to_csv_append(df, outfile)
    print(df.shape)


# -

# ## Links

def get_df_link_from_declaration(df_declaration, is_convention=False):
    columns_mapping = {
        'annee': 'year',
        'categorie_precise': 'category',
        'date': 'date',
        'declaration_id': 'link_id',
        'detail': 'details',
        'identifiant_convention': '',
        'identifiant_entreprise': 'source_organization_id',
        'montant_ttc': 'value_total_amount',
        'type_declaration': 'type',
        'catégorie_beneficiaire': 'recipient_entity_type',
    }
    if is_convention:
        columns_mapping.update({'related_activity_id': 'related_activity_id'})

    columns_default_values = {
        'currency': "EUR",
        'is_aggregated': False,
        'number_of_aggregated_recipients': 1,
        'publication_id': 'france',
    }
    df = select_and_rename_columns(df_declaration, columns_mapping)
    df = create_columns_with_default_values(df, columns_default_values)
    df['recipient_entity_id'] = 'fr_recipient_' + df['link_id']
    df['value_total_amount_eur'] = df['value_total_amount']
    df['date'] = df['date'].map(to_iso_date)

    df = align_to_columns_list(df, link_columns)
    return df


# %%time
outfile = p_join(sunshine_europe_dump_dir, 'link__remuneration.csv')
if os.path.exists(outfile):
    os.remove(outfile)
for df in pd.read_csv(p_join(dump_dir, 'declaration_remuneration.csv.gz'),
                      sep=';', dtype=str, chunksize=10 ** 6, nrows=nrows):
    df = get_df_link_from_declaration(df)
    to_csv_append(df, outfile)
    print(df.shape)

# %%time
outfile = p_join(sunshine_europe_dump_dir, 'link__avantage.csv')
if os.path.exists(outfile):
    os.remove(outfile)
for df in pd.read_csv(p_join(dump_dir, 'declaration_avantage.csv.gz'),
                      sep=';', dtype=str, chunksize=10 ** 6, nrows=nrows):
    df = get_df_link_from_declaration(df)
    to_csv_append(df, outfile)
    print(df.shape)

# ## Activities & activity entities

activity_columns = [
    'activity_id',
    'name',
    'organization_id',
    'organization_name',
    'type',
    'details',
    'start_date',
    'end_date',
    'location',
    'address',
    'city',
    'state_province_region',
    'postal_code',
    'country',
    'latitude',
    'longitude'
]


def get_df_activity_and_organizer_from_declaration_convention(df):
    activity_columns_mapping = {
        'event_date': 'start_date',
        'declaration_id': 'declaration_id',
        'detail': 'details',
        'entreprise_émmetrice': 'disclosing_company',
        'identifiant_entreprise': 'disclosing_company_id',
        'event': 'name',
        'place': 'location',
        'organizer': 'organization_name',
        'related_activity_id': 'activity_id'
    }
    activity_organizer_columns_mapping = {
        'organization_id': 'entity_id',
        'organization_name': 'full_name',
    }
    activity_organizer_columns_default_values = {
        'is_person': False,
        'entity_type': 'Industry'
    }

    df = select_and_rename_columns(df[df["has_event"]], activity_columns_mapping)
    df["organization_id"] = 'fr_activity_organizer_' + df["declaration_id"]

    df["organization_name"] = df["organization_name"].fillna("")

    mask_no_organizer = df["organization_name"] == ""

    mask_organizer_same_disclosing = df.apply(
        lambda s:
        (s["organization_name"].upper() in s["disclosing_company"].upper())
        | (s["disclosing_company"].upper() in s["organization_name"].upper()),
        axis=1
    )
    mask_no_new_organizer = mask_no_organizer | mask_organizer_same_disclosing

    df.loc[mask_no_new_organizer, "organization_name"] = df["disclosing_company"]
    df.loc[mask_no_new_organizer, 'organization_id'] = df['disclosing_company_id']

    df_activity = align_to_columns_list(df, activity_columns)

    df_organizer = df_activity[~mask_no_new_organizer].copy()
    df_organizer = select_and_rename_columns(df_organizer, activity_organizer_columns_mapping)
    df_organizer = create_columns_with_default_values(df_organizer, activity_organizer_columns_default_values)
    df_organizer = align_to_columns_list(df_organizer, entity_columns)

    return df_activity, df_organizer


# +
# %%time

link_outfile = p_join(sunshine_europe_dump_dir, 'link__convention.csv')
if os.path.exists(link_outfile):
    os.remove(link_outfile)

activity_outfile = p_join(sunshine_europe_dump_dir, 'activity__convention.csv')
if os.path.exists(activity_outfile):
    os.remove(activity_outfile)

activity_organizer_outfile = p_join(sunshine_europe_dump_dir, 'entity__convention_activity_organizer.csv')
if os.path.exists(activity_organizer_outfile):
    os.remove(activity_organizer_outfile)

for df in pd.read_csv(p_join(dump_dir, 'declaration_convention.csv.gz'),
                      sep=';', dtype=str, chunksize=10 ** 6, nrows=nrows):
    df = prepare_df_convention(df)

    df_link = get_df_link_from_declaration(df.copy(), is_convention=True)
    to_csv_append(df_link, outfile)
    print("link", df_link.shape)

    df_activity, df_organizer = get_df_activity_and_organizer_from_declaration_convention(df)

    to_csv_append(df_activity, activity_outfile)
    print("activity", df_activity.shape)

    to_csv_append(df_organizer, activity_organizer_outfile)
    print("activity_organizer", df_organizer.shape)


# +


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), os.path.join(path, '..')))


zipf = zipfile.ZipFile(sunshine_europe_dump_dir + '.zip', 'w', zipfile.ZIP_DEFLATED)
zipdir(sunshine_europe_dump_dir, zipf)
zipf.close()

# +

shutil.rmtree(sunshine_europe_dump_dir)
# -
