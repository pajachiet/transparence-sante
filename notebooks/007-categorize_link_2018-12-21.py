# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Setup

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 100
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100
from pprint import pprint

import sys
ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import ROOTED_CLEANED_DATA_DIR, ROOTED_RAW_DATA_DIR
import src.constants.csv as csv

# # Load data 

DATA_DIR = ROOTED_CLEANED_DATA_DIR
NROWS=10**5

# %%time
csv_path = os.path.join(DATA_DIR, csv.ADVANTAGE_CSV)
df_avantage = pd.read_csv(csv_path, sep=csv.DEFAULT_SEPARATOR, nrows=NROWS, 
                          usecols=['detail'])

# # Map to ontology

from src.constants import ontology
from src.specific.map_to_ontology import get_strict_mapper_to_category, get_similarity_mapper_to_category, \
get_precise_category, get_general_category
ontology.print_tree()

from anytree import Node
Node.__str__ = lambda self: self.name_fr

strict_mapping = get_strict_mapper_to_category()
similarity_mapping = get_similarity_mapper_to_category(df_avantage.detail)
df_avantage['categorie_strict'] = df_avantage.detail.map(strict_mapping)
df_avantage['categorie_similarity'] = df_avantage.detail.map(similarity_mapping)

df_avantage.pipe(get_precise_category, 'categorie_strict').value_counts()

df_avantage.pipe(get_general_category, 'categorie_strict').value_counts()

# ## Study mapping

is_mapping_sucessfull = df_avantage["categorie_similarity"] != ontology.UNSUCESSFULL_MAPPING

is_mapping_strict_sucessfull = df_avantage["categorie_strict"] != ontology.UNSUCESSFULL_MAPPING

df_avantage[~is_mapping_sucessfull].detail.value_counts()

df_avantage[~is_mapping_strict_sucessfull].drop_duplicates().head(20)

(df_avantage[~is_mapping_sucessfull]
 .detail
 .value_counts()
 .head(20)
)


