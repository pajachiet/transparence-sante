# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
# ---

# # Initialisation

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
from pprint import pprint
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 70
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100
import matplotlib.pyplot as plt
from matplotlib_venn import venn2

ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import CLEANED_DATA_DIR, RAW_DATA_DIR
import src.constants.csv as csv

# # Lecture des données

# +
df_entreprise = pd.read_csv('data/raw/entreprise.csv')

df_association = pd.read_csv('data/raw/association_entreprise_groupe.csv').dropna(subset=['identifiant'])

len(df_entreprise)

len(df_association)

assert not df_association.identifiant.duplicated().sum()

assert not (set(df_association.identifiant) - set(df_entreprise.identifiant))


common_columns = ['identifiant', 'secteur', 'denomination_sociale']
association_columns = ['identifiant',
                       'Indépendant',
                       'groupe', 
                       'sous_groupe', 
                       'Associé par', 
                       'Validé par',
                       'Justification : source ou  commentaire'
                      ]
(df_entreprise[common_columns]
 .merge(df_association[association_columns], on='identifiant', how='outer')
 .sort_values('denomination_sociale')
 .to_csv('data/raw/association_entreprise_groupe_with_missing_entreprise.csv', index=False)
)
# -



df_association.columns

l

# +
DATA_DIR = RAW_DATA_DIR

path = os.path.join(DATA_DIR, csv.HEALTH_PROFESSIONAL_ACTIVITY_RAW_CSV)
# %time df = pd.read_csv(path, sep='|', dtype='str')
# -

PROFESSIONAL_ID = 'Identifiant PP'
EMPLOYMENT_STATUS = 'Libellé mode exercice'
ORGANIZATION_ID = 'Identifiant technique de la structure'



id_column = PROFESSIONAL_ID
non_id_columns = [EMPLOYMENT_STATUS, ORGANIZATION_ID]
full_na_rows = df.head(10000)[non_id_columns].isna().all(axis=1)
full_na_rows.sum()

df.head(10000)[full_na_rows][[EMPLOYMENT_STATUS, ORGANIZATION_ID]].drop_duplicates()


df.head(2)

PROFESSIONAL_ID = 'Identifiant PP'
EMPLOYMENT_STATUS = 'Libellé mode exercice'
ORGANIZATION_ID = 'Identifiant technique de la structure'
mask_null = df[[EMPLOYMENT_STATUS, ORGANIZATION_ID]].isna().all(axis=1)
mask_null.sum()


mask_null = df[[ORGANIZATION_ID]].isna().all(axis=1)
mask_null.sum()

mask = df[EMPLOYMENT_STATUS].isna() & df[ORGANIZATION_ID].notnull()
print(mask.sum())
df[mask]

mask = df[EMPLOYMENT_STATUS].notnull() & df[ORGANIZATION_ID].isna()
print(mask.sum())
df[mask]['Libellé profession'].value_counts()

df[mask][EMPLOYMENT_STATUS].value_counts()

l = list(df.columns)
print(PROFESSIONAL_ID in l)
l.remove(PROFESSIONAL_ID)
print(PROFESSIONAL_ID in l)

# ?l.remove

df[df[PROFESSIONAL_ID] == '10004972005']

df[mask_null][PROFESSIONAL_ID].value_counts()

df[mask_null][EMPLOYMENT_STATUS].value_counts(dropna=False)

df[EMPLOYMENT_STATUS].value_counts(dropna=False)

df_ban = pd.read_csv('data/raw/BAN_licence_gratuite_repartage/BAN_licence_gratuite_repartage_01.csv', sep=';', dtype='str')

(df_ban.numero == "900").sum()

df_ban[df_ban.nom_commune.str.startswith('Viria') & 
       df_ban.nom_voie.str.startswith('route de paris') & 
       (df_ban.numero == "900")
      ]

# # Recherche des doublons

df.head()

# +
from src.constants import column

(df[[column.ORGANIZATION_ID, column.ADDRESS, column.POSTAL_CODE]].head(100)
    .to_csv('data/cleaned/adresse_etablissement_sante.csv', index=False)
)
# -


df_head = (df[[column.ORGANIZATION_ID, column.ADDRESS, column.POSTAL_CODE]].head(100)
    .to_string()
)

# +
import requests

files = {
    'columns': (None, 'adresse'),
    'data': ('csv_file', open('data/cleaned/adresse_etablissement_sante.csv', 'rb')),
}

response = requests.post('http://devapi-adresse.data.gouv.fr/search/csv/', files=files)

from io import StringIO
df_response = pd.read_csv(StringIO(response.text))

df_response

# +
import requests

files = {
    'columns': (None, 'adresse'),
    'data': ('csv_file', open('data/cleaned/adresse_etablissement_sante.csv', 'rb')),
}

response = requests.post('https://api-adresse.data.gouv.fr/search/csv/', files=files)

from io import StringIO
df_response = pd.read_csv(StringIO(response.text))

df_response

# +
response = requests.get("https://api-adresse.data.gouv.fr/search/?q=900 Route DE PARIS Viriat")

response.json()
# -

df[df.result_score < 0.7]


request.post("https://api-adresse.data.gouv.fr/search/csv/", {"columns":'adresse', 'data':})
! curl -X POST -F columns=adresse  -F data=@data/cleaned/adresse_etablissement_sante.csv https://api-adresse.data.gouv.fr/search/csv/

df.adresse.to_csv('data/cleaned/adresse_etablissement_sante.csv', index=False)





identifiant = 'Identifiant technique de la structure'

def get_subset_multiple_values_same_id(df, column):
    n_values_of_column_for_id = df.groupby(identifiant)[column].nunique()
    id_with_multiple_value_of_column = set(n_values_of_column_for_id[n_values_of_column_for_id > 1].index)
    mask_multiple_names = df[identifiant].isin(id_with_multiple_value_of_column)
    df_multiple_values_same_id = df[mask_multiple_names].drop_duplicates(subset=[identifiant, column])
    print(df_multiple_values_same_id.shape[0], 'duplicated lines (divide by 2 for the number of id)')
    return (df_multiple_values_same_id.sort_values(identifiant))

df = df.dropna(subset=[identifiant])

from src.constants import column
from src.generic.validate import get_checker_values_are_unique_for_one_id
columns = [
    'Numéro SIRET site',
    'Numéro FINESS site',
    'Numéro SIREN site',
    'Numéro FINESS établissement juridique',
    'Raison sociale site',
    'Enseigne commerciale site',
    # column.ADDRESS,
]

check_columns_are_unique = get_checker_values_are_unique_for_one_id(identifiant, columns)
check_columns_are_unique(df)

get_subset_multiple_values_same_id(df, 'Numéro SIRET site')

df[columns].duplicated().sum()





# +
identifiant = 'Identifiant technique de la structure'


name_columns = [
    'nom',
    'prénom',
]
id_name_columns = [identifiant] + name_columns


columns = [
    identifiant,
    'civilite',
    'nom',
    'prénom',
    'categorie_professionnelle',
    'profession',
    'specialité',
    'commune'
]
# -

df[df.identifiant.str.contains('A')]

df[df.nom_prénom.duplicated(keep=False)].sort_values('nom_prénom').head(20)

from src.constants import column
from src.specific.health_directory import concat_columns

import string
def concat_columns(s1, s2, sep=', '):
    return (s1.fillna('')
            .str.cat(s2.fillna('').values, sep=sep)
            .str.strip(string.whitespace + sep)
           )

df = df[df.profession == 'Médecin']

s = pd.Series(["a","b","c","a"], dtype="category")
s.astype(str)

s

# +
df['search_column'] = df.nom_prénom

mask_duplicated = df['search_column'].duplicated(keep=False)
print(mask_duplicated.sum())
s_masked_dedup = concat_columns(df.loc[mask_duplicated, 'search_column'], df.loc[mask_duplicated, 'profession'], sep=', ')
df['search_column'].update(s_masked_dedup)


mask_duplicated = df['search_column'].duplicated(keep=False)
print(mask_duplicated.sum())
s_masked_dedup = concat_columns(df.loc[mask_duplicated, 'search_column'], df.loc[mask_duplicated, 'specialité'], sep=', ')
df['search_column'].update(s_masked_dedup)

mask_duplicated = df['search_column'].duplicated(keep=False)
print(mask_duplicated.sum())
s_masked_dedup = concat_columns(df.loc[mask_duplicated, 'search_column'], df.loc[mask_duplicated, 'commune'])
df['search_column'].update(s_masked_dedup)


mask_duplicated = df['nom_prénom'].duplicated(keep=False)
print(mask_duplicated.sum())
s_masked_dedup = concat_columns(df.loc[mask_duplicated, 'search_column'], df.loc[mask_duplicated, 'identifiant'], sep=', n°')
df['search_column'].update(s_masked_dedup)
# -

df[df.profession == 'Médecin'][df.nom_prénom.duplicated(keep=False)].sort_values('search_column')[columns + ['search_column']]

df.loc[mask_duplicated, 'nom_prénom'].sort_values().head()

concat_columns(df.loc[mask_duplicated, 'nom_prénom'], df.loc[mask_duplicated, 'profession']).sort_values()

COLUMNS_WITH_UNIQUE_VALUES

df.search_string.value_counts()

df_unique = df.drop_duplicates(['identifiant', column.ID_TYPE] + COLUMNS_WITH_UNIQUE_VALUES)

mister = (df_unique[df_unique.nom == 'ABOU TAAM']
          [df_unique.duplicated(COLUMNS_WITH_UNIQUE_VALUES, keep=False)]
          .sort_values(['nom', 'prénom'])
          .drop_duplicates(['identifiant', column.ID_TYPE] + COLUMNS_WITH_UNIQUE_VALUES)
         )
print("_{}_".format(mister.loc[357879].identifiant))
print("_{}_".format(mister.loc[1704089].identifiant))


type(mister.loc[357879].identifiant), type(mister.loc[1704089].identifiant)

df.duplicated(subset=['identifiant'] + COLUMNS_WITH_UNIQUE_VALUES).sum()

df.duplicated(subset=['identifiant']).sum()

df.profession.value_counts(dropna=False)

df.specialité.value_counts(dropna=False)

def get_subset_multiple_values_same_id(column):
    n_values_of_column_for_id = df.groupby(identifiant)[column].nunique()
    id_with_multiple_value_of_column = set(n_values_of_column_for_id[n_values_of_column_for_id > 1].index)
    mask_multiple_names = df[identifiant].isin(id_with_multiple_value_of_column)
    df_multiple_values_same_id = df[mask_multiple_names].drop_duplicates(subset=[identifiant, column])
    print(df_multiple_values_same_id.shape[0], 'duplicated lines (divide by 2 for the number of id)')
    return (df_multiple_values_same_id.sort_values(identifiant))

# ## catégorie professionnelle

df.drop_duplicates('identifiant').profession.value_counts()

df.commune.value_counts(dropna=False)

df_dupli = get_subset_multiple_values_same_id("commune")

df_dupli.groupby('identifiant').commune.nunique().value_counts()

get_subset_multiple_values_same_id("nom")

get_subset_multiple_values_same_id("prénom")[columns]

get_subset_multiple_values_same_id("categorie_professionnelle")[columns]

get_subset_multiple_values_same_id("profession")[columns]

get_subset_multiple_values_same_id("specialité")[columns]

# # Dédoublonnage

# ### Coup d'oeil

s_nb_prof = df.groupby('identifiant').profession.nunique()
s_nb_prof.value_counts()

df[df.identifiant.isin(s_nb_prof[s_nb_prof == 2].index)].sort_values('identifiant')[columns]

s_nb_spe = df.groupby('identifiant').specialité.nunique()
s_nb_spe.value_counts()

df[df.categorie_professionnelle == 'Militaire'].index

df.specialité.value_counts()

multi_value = set(s_nb_spe[s_nb_spe == 2].index)
militaires = set(df[df.categorie_professionnelle == 'Militaire'].identifiant)
militaires = set(df[df.specialité == 'Médecine Générale'].identifiant)
(df[df.identifiant.isin(multi_value - militaires)]
.sort_values('identifiant')
 [columns])

df[df.identifiant == "10000396605"]

df[name_columns].head(3)

# Les médecin sans spécialité sont des étudiants

df.specialité.value_counts()

columns = [
    'identifiant',
    'civilité_exercice',
    'nom',
    'prénom',
    'profession',
    'catégorie_professionnelle',
    'specialité',
    'type_spécialité',
    'raison_sociale_site',
    'enseigne_commerciale_site',
    'identifiant_structure',
    'finess_etablissement_juridique',
    'finess_site',
    'siren_site',
    'siret_site',
    'complément_destinataire_structure',
    'complément_point_géographique_structure',
    'numéro_voie_structure',
    'indice_répétition_voie_structure',
    'libellé_type_de_voie_structure',
    'libellé_voie_structure',
    'libellé_commune_structure',
    'code_postal_structure',
    'bureau_cedex_structure',
    'mention_distribution_structure',
    'libellé_pays_structure',
    'adresse_email_structure',
    'adresse_bal_mssanté',
    'télécopie_structure',
    'téléphone_2_structure',
    'téléphone_structure'
]


df[df.specialité.isna() & (df.profession == 'Médecin')].categorie_professionelle.value_counts()


df[df.specialité == 'Médecine Générale'][df.categorie_professionelle == 'Civil'].identifiant.nunique()

# 0 doublons complet

assert df.duplicated().sum() == 0

# Nombreux doublons sur l'identifiant

df.duplicated(subset=['identifiant']).value_counts()

# ## Plusieurs noms ou prénom pour le même identifiant
# Solution : 
# - Bonne : récupérer le set des nom, et les concaténer
# - Réalisé : prendre le nom le plus 'grand' au sens lexicographique

get_subset_multiple_values_same_id("nom")[columns].head(6)

get_subset_multiple_values_same_id("prénom")[columns].head(6)

# ### Traitement

# %time df_nom = df.groupby('identifiant').nom.max()

# %time df_prénom = df.groupby('identifiant').prénom.max()

# ## Plusieurs catégories professionnelles
# - Traitement: n'en garder qu'une. Par priorité étudiant, puis civil, puis miliaire

df_dupli = get_subset_multiple_values_same_id("categorie_professionnelle")[id_name_columns + ["profession", "categorie_professionnelle"]]
df_dupli.head(10)

df_dupli.sort_values(["categorie_professionnelle"]).drop_duplicates(['identifiant']).sort_values('identifiant')

# +
sorted_professional_category = [
    'Etudiant',
    'Civil',
    'Militaire',
]

df_dupli["categorie_professionnelle"] = pd.Categorical(
    df_dupli['categorie_professionnelle'], 
    categories=sorted_professional_category, 
    ordered=True
)
# -

# ## Plusieurs professions
# Faire un choix arbitraire, ou selon une hiérarchie (catégorie avec plus de liens devant), ou selon distinction étudiant-civil

(get_subset_multiple_values_same_id("profession")
[columns]
 .head(10))

# ### Traitement

# +
sorted_profession = [
    'Pédicure-Podologue',
    'Masseur-Kinésithérapeute',
    'Sage-Femme',
    'Chirurgien-Dentiste',
    'Pharmacien',
    'Médecin',
]

df['profession'] = pd.Categorical(
    df['profession'], 
    categories=sorted_profession, 
    ordered=True
)
# -

# %%time 
df_pro = df.groupby("identifiant", sort=False, as_index=False).agg({
    'profession': max
})

df_pro.groupby('identifiant').profession.nunique(dropna=False).value_counts()

# ## Plusieurs spécialités x469 lignes
# - Regroupement des spé de med gé
# - pharmacien -> le plus spécifique "Expérience prat. art. R.5124-16 du CSP Exploitant" plutôt que "Fabriquant"
# - médecin -> le plus spécifique (non med gé)
#

(get_subset_multiple_values_same_id("specialité")
[columns]
)

# ### Traitement

# %%time 
df_spe = df.groupby("identifiant", sort=False, as_index=False).agg({
    'specialité': max
})

# # Objectif
#
# 1. Créer un identifant "lisible" type nom + prénom + spécialité + lieu + rpps => pour la recherche
# 2. Dédoublonner à 1 ligne par RPPS

df['code_postal_in_codes_postaux'] = df.code_postal_structure.map(normalize_code_postal).isin(codes_postaux)
df['code_postal_in_codes'] = df.code_postal_structure.map(normalize_code_postal).isin(codes)

df['code_postal_in_codes_postaux'] = df.code_postal_structure.map(normalize_code_postal).isin(codes_postaux)
df['code_postal_in_codes'] = df.code_postal_structure.map(normalize_code_postal).isin(codes)

# # Dédoublonage

# +
sorted_professional_category = [
    'Etudiant',
    'Civil',
    'Militaire',
]

df["categorie_professionnelle"] = pd.Categorical(
    df['categorie_professionnelle'], 
    categories=sorted_professional_category, 
    ordered=True
)
df = df.sort_values(["categorie_professionnelle"]).drop_duplicates(['identifiant'])
# -



# +
sorted_profession = [
    'Pédicure-Podologue',
    'Masseur-Kinésithérapeute',
    'Sage-Femme',
    'Pharmacien',
    'Chirurgien-Dentiste',
    'Médecin',
]

df['profession'] = pd.Categorical(
    df['profession'], 
    categories=sorted_profession, 
    ordered=True
)
# -

# ### Préparation des colonnes à dédoublonner

def get_sorted_categorical_by_frequency(df, column, max_is_rarest=True):
    # if max_is_rarest : 
    # - Rarest value will be the max in categorical order, 
    # - min value will be NaN, 2nd min value will be the most frequent
    # else : 
    # - Most frequent value will be the max in categorical order, 
    # - min value will be NaN, 2nd min value will be the rarest

    is_ascending = not max_is_rarest
    values_sorted_by_descending_frequency = df[column].value_counts().sort_values(ascending=is_ascending).index
    return pd.Categorical(
        df[column], 
        categories=values_sorted_by_descending_frequency, 
        ordered=True
    )


list(df['profession'].value_counts().sort_values().index)

list(df['specialité'].value_counts().sort_values().index)

# Duplicate specialty are mostly from doctors that were soldier with general medicine specialty
# The rarest is more precise
df['specialité'] = get_sorted_categorical_by_frequency(df, 'specialité') 

# +
def normalize_code_postal(code):
    try:
        result = str(int(code)).zfill(5)
    except ValueError:
        result = np.nan
    return result

df['code_postal_structure'] = df.code_postal_structure.map(normalize_code_postal)
df['code_postal_structure'] = get_sorted_categorical_by_frequency(df, 'code_postal_structure', max_is_rarest=False)
# -

from src.specific.functions import normalize_city

df['libellé_commune_structure'] = df.libellé_commune_structure.pipe(normalize_city)
df['libellé_commune_structure'] = get_sorted_categorical_by_frequency(df, 'libellé_commune_structure')

# ### Dédoublonnage

# %%time
df_treated = df.groupby("identifiant", sort=False, as_index=False).agg({
    'nom': max,
    'prénom': max,
    'profession': max,
    'specialité': max,
#    'code_postal_structure': max,
#    'libellé_commune_structure': max,
})

df_treated.isna().sum()

df_treated_path = os.path.join(DATA_DIR, 'deduplicated_rpps.csv')
df_treated.to_csv(df_treated_path, sep=';', index=False)

# ### Étude résultat

assert df_treated.identifiant.duplicated().sum() == 0

df_treated.shape

# #### Nombre d'homonymes

df_treated.duplicated(subset=name_columns).sum()

# #### Nombre de médecins généralistes

df_treated[df_treated.specialité == 'Médecine Générale'].shape

# #### Nombre de doublons en ajoutant la spécialité

df_treated.duplicated(subset=name_columns + ['specialité']).sum()

# #### Nombre de doublons en ajoutant la spécialité ET la commune

df_treated.duplicated(subset=name_columns + ['specialité', 'libellé_commune_structure']).sum()

# #### Visualisation des doublons "ultimes", qui ont un code postal et une structure

df_treated[df_treated.duplicated(subset=name_columns + ['specialité', 'libellé_commune_structure'])].dropna()

# # Création d'un identifiant textuel unique pour la recherche

df_treated.identifiant.map(str).str[0].value_counts()







# ## Code postal

# ### Fichiers de codes postaux avec des coordonnées géographiques

# #### poste

df_poste = pd.read_csv("data/raw/laposte_hexasmal.csv", sep=';')

print(df_poste.shape)
df_poste.sample(2)

df_poste.Code_commune_INSEE.map(str).map(len).value_counts()

codes_postaux_poste = set(df_poste.Code_postal.map(str))
codes_insee_poste = set(df_poste.Code_commune_INSEE.map(str))

# #### EU Circos

df_circo = pd.read_csv("data/raw/EUCircos_Regions_departements_circonscriptions_communes_gps.csv", sep=';')
df_circo = df_circo.dropna(subset=['latitude', 'longitude'])

print(df_circo.shape)
df_circo.sample(2)

codes_postaux_circo = set(df_circo.codes_postaux)
codes_insee_circo = set(df_circo.code_insee.map(str))

# #### Correspondance insee postal

df_corres = pd.read_csv("data/raw/correspondances-code-insee-code-postal.csv", sep=';')

print(df_corres.shape)
df_corres.sample(2)

df_corres.Commune.nunique()

# #### Intersection entre les ensembles

code_postaux_corres = set(df_corres['Code Postal'])
code_insee_corres = set(df_corres['Code INSEE'])

venn2([codes_postaux_poste, code_postaux_corres], set_labels=["poste", "corres"]);

venn2([codes_postaux_poste, codes_postaux_circo], set_labels=["codes postaux poste", "codes postaux circo"]);

venn2([codes_insee_poste, codes_insee_circo], set_labels=["codes insee poste", "codes insee circo"]);

codes_postaux = codes_postaux_poste | codes_postaux_circo

codes_insee = codes_insee_poste | codes_insee_circo

# #### Choix final

codes = codes_postaux | codes_insee

# ### Normalisation code 

# %%time 
df_code = df.groupby("identifiant", sort=False, as_index=False).agg({
    'code_postal_structure': max
})

df_code = df.code_postal_structure.map(normalize_code_postal)
df_code = df_code[df_code.map(len) > 0]

df_code.isin(codes).value_counts()

df['code_postal_in_codes_postaux'] = df.code_postal_structure.map(normalize_code_postal).isin(codes_postaux)
df['code_postal_in_codes'] = df.code_postal_structure.map(normalize_code_postal).isin(codes)

(get_subset_multiple_values_same_id("code_postal_structure")
[id_name_columns + ["code_postal_structure"]]
)

# ## commune

df_poste.Nom_commune.nunique()

df_treated.libellé_commune_structure.nunique()


# #### Lien pour coordonnées GPS

df_annuaire_commune = df.libellé_commune_structure.pipe(normalize_city)
set_annuaire_commune = set(df_annuaire_commune)
print(df.libellé_commune_structure.nunique(dropna=False), len(set_annuaire_commune))

set_annuaire_commune = set(df_treated.libellé_commune_structure)

df_circo_commune = df_circo.nom_commune.pipe(normalize_city)
set_circo_commune = set(df_circo_commune)
print(df_circo.nom_commune.nunique(dropna=False), len(set_circo_commune))

df_poste_commune = df_poste.Nom_commune.pipe(normalize_city)
set_poste_commune = set(df_poste_commune)
print(df_poste.Nom_commune.nunique(dropna=False), len(set_poste_commune))

venn2([set_circo_commune, set_poste_commune], ('communes circo', 'communes poste'));

set_communes = set_circo_commune | set_poste_commune | set(main_cities)

venn2([set_annuaire_commune, set_communes], ('communes annuaire', 'communes poste & autres'));

df_annuaire_commune.isna().value_counts()

mask_commune = df_annuaire_commune.dropna().isin(set_communes)
mask_commune.value_counts()

df_annuaire_commune.dropna()[~mask_commune].value_counts().head(10)

from difflib import get_close_matches

get_close_matches('La Hague', set_poste_commune, cutoff=0.8)

df['libellé_commune_structure'] = df.libellé_commune_structure.pipe(normalize_city)
df['libellé_commune_structure'] = get_sorted_categorical_by_frequency(df, 'libellé_commune_structure')

(get_subset_multiple_values_same_id("libellé_commune_structure")
[id_name_columns + ["libellé_commune_structure"]]
 .head(10))

# %%time 
df_com = df.groupby("identifiant", sort=False, as_index=False).agg({
    'libellé_commune_structure': max
})

df_com.groupby('identifiant').libellé_commune_structure.nunique(dropna=False).value_counts()




