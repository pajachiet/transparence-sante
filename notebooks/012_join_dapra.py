# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.2
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
#   language_info:
#     codemirror_mode:
#       name: ipython
#       version: 3
#     file_extension: .py
#     mimetype: text/x-python
#     name: python
#     nbconvert_exporter: python
#     pygments_lexer: ipython3
#     version: 3.6.5
# ---

# # Initialisation

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys
from pprint import pprint
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 70
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100
import matplotlib.pyplot as plt
from matplotlib_venn import venn2

os.chdir("../data/raw/bgoupil/")

# # Lecture fichiers

medge_file = "Liste MG libéraux avec Groupe 181113"
dapra_file = "DA_PRA_R_201612 181113"

df_dapra = pd.read_csv(dapra_file + '.csv', sep=';', encoding = "ISO-8859-1", dtype='str')

df_medge = pd.read_csv(medge_file + '.csv', sep=';', dtype='str')

# ### Colonnes de jointure

medge_column = ['Nom', 'Prenom', 'CP']
dapra_column = ['PFS_EXC_NOM', 'IPP_PFS_PRN', 'PFS_COD_POS']

# ### Coup d'oeil

df_dapra.head(3)

df_medge.head(3)

# +
df_join = df_medge.drop_duplicates(subset=medge_column, keep=False).merge(
    df_dapra.drop_duplicates(subset=dapra_column, keep=False), 
    how='outer',
    left_on=medge_column, 
    right_on=dapra_column, 
    indicator=True,
    validate='one_to_one')

df_join._merge.value_counts()
# -

# Qualité jointure **sans nettoyage**
# - both = jointure réussie
# - left_only = ligne medge sans ligne dapra
# - right_only = ligne dapra san ligne medge

# # Nettoyage

# ## Normalisation longeur code postaux
# Certains codes postaux n'ont pas le 0 initial

df_medge.CP.map(len).value_counts()

df_dapra.PFS_COD_POS.map(len).value_counts()

# ### Nettoyage

df_medge.CP = df_medge.CP.str.zfill(5)

# ## Normalisation nom prénom

# +
import string
import unicodedata
import re

def replace_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')
def get_non_authorized_chars_remover(authorized_chars):
    def non_authorized_chars_remover(s):
        # Always keep space ' '
        return re.sub('[^{}]'.format(authorized_chars + ' '), ' ', s)
    return non_authorized_chars_remover

def normalize_string_series(series):
    return (series
            .fillna('').map(str)
            .map(replace_accents)
            .map(get_non_authorized_chars_remover(string.ascii_letters))
            .str.split().str.join(' ')
            )

def normalize_name(s_names):
    return s_names.pipe(normalize_string_series).str.upper()

# +
df_dapra.PFS_EXC_NOM = normalize_name(df_dapra.PFS_EXC_NOM)
df_dapra.IPP_PFS_PRN = normalize_name(df_dapra.IPP_PFS_PRN)

df_medge.Nom = normalize_name(df_medge.Nom)
df_medge.Prenom = normalize_name(df_medge.Prenom)
# -

# # Suppression des doublons sur la clé : nom prénom cp

medge_column = ['Nom', 'Prenom', 'CP']
dapra_column = ['PFS_EXC_NOM', 'IPP_PFS_PRN', 'PFS_COD_POS']

# ## Pas d'unicité avec cette clé multicolonne

# Unicité du RPPS ok

df_medge.RPPS.duplicated().sum()

# 3 doublons dans le fichier medgé avec ces colonnes

df_medge[medge_column].duplicated().sum()

# 564 doublons dans le fichier dapra avec ces colonnes

df_dapra[dapra_column].duplicated().sum()

# ## Suppression des lignes avec doublon

df_medge = df_medge.drop_duplicates(subset=medge_column, keep=False)
df_dapra = df_dapra.drop_duplicates(subset=dapra_column, keep=False)

# ### Vérification

df_medge[medge_column].duplicated().sum()

df_dapra[dapra_column].duplicated().sum()

# # Jointure

# +
df_join = df_medge.merge(
    df_dapra, 
    how='outer',
    left_on=medge_column, 
    right_on=dapra_column, 
    indicator=True,
    validate='one_to_one')

df_join._merge.value_counts()
# -

# Qualité jointure
# - both = jointure réussie
# - left_only = ligne medge sans ligne dapra
# - right_only = ligne dapra san ligne medge

# ### Coup d'oeil aux lignes sans jointure réussie

df_join[df_join._merge == 'left_only']

df_join[df_join._merge == 'right_only']

# # Sauvegarde

df_medge.to_csv(medge_file + '_normalized.csv', index=False, sep=';')

df_dapra.to_csv(dapra_file + '_normalized.csv', index=False, sep=';')

df_join.to_csv("medge_dapra_join.csv", index=False, sep=';')


