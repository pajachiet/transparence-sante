# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.2
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
#   language_info:
#     codemirror_mode:
#       name: ipython
#       version: 3
#     file_extension: .py
#     mimetype: text/x-python
#     name: python
#     nbconvert_exporter: python
#     pygments_lexer: ipython3
#     version: 3.6.5
# ---

# # Setup

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

# +
import os
import sys
import numpy as np
import pandas as pd
import pandas_profiling
pd.options.display.max_rows = 30
pd.options.display.max_columns = 100

pd.options.display.max_colwidth = 100
# -

import sys
ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

from src.constants.directories import ROOTED_CLEANED_DATA_DIR
import src.constants.csv as csv

# +
DATA_DIR = ROOTED_CLEANED_DATA_DIR

cleaned_remuneration_path = os.path.join(DATA_DIR, csv.REMUNERATION_CSV)
df_remuneration = pd.read_csv(cleaned_remuneration_path, sep=csv.DEFAULT_SEPARATOR)

cleaned_convention_path = os.path.join(DATA_DIR, csv.CONVENTION_CSV)
df_convention = pd.read_csv(cleaned_convention_path, sep=csv.DEFAULT_SEPARATOR)

cleaned_avantage_path = os.path.join(DATA_DIR, csv.ADVANTAGE_CSV)
df_avantage = pd.read_csv(cleaned_avantage_path, sep=csv.DEFAULT_SEPARATOR)

cleaned_company_path = os.path.join(DATA_DIR, csv.COMPANY_CSV)
df_company = pd.read_csv(cleaned_company_path, sep=csv.DEFAULT_SEPARATOR)
# -

print(df_remuneration.shape)
df_remuneration.sample(2)

print(df_convention.shape)
df_convention.sample(2)

print(df_avantage.shape)
df_avantage.sample(2)

print(df_company.shape)
df_company.sample(2)

# # Objectif  : lier les rémunérations et avantages aux conventions 
#
# Résultat attendu :
# - Ajout d'un identifiant de jointure : identifiant_entreprise + identifiant_convention
# - Opération de groupement / jointure
#     - Correction des montants de conventions : 
#         - retirer les montants déclarés ailleurs, sans aller sous 0
#         - Distinction des conventions au montant caché (null), ou au montant déclaré ailleurs (0)
#     - Compter le nombre d'avantages et de rémunérations liés

# ## Etude préliminaire

# ### Pas de doublons de n° de convention si l'on utilise l'identifiant d'entreprise 
#
# Mais doublons si on prend la filiale ou la société mère

nb_conventions_par_id = (df_convention.identifiant_entreprise + '|' +  df_convention.identifiant_convention).value_counts()
mask_doublons = nb_conventions_par_id >1 

assert (nb_conventions_par_id == 1).all()

df_convention[(df_convention.filiale_déclarante == '3M Deutschland GmbH') & 
              (df_convention.identifiant_convention == '5')]

# ### Beaucoup d'avantages sans convention, aucune rémunération sans convention

df_avantage.identifiant_convention.isna().value_counts()

df_remuneration.identifiant_convention.isna().value_counts()

# ## Jointure

# ### Création identifiant convention

def build_identifiant_convention(df):
    return df.identifiant_entreprise + '|' +  df.identifiant_convention

df_avantage["identifiant_convention"] = build_identifiant_convention(df_avantage)

df_remuneration["identifiant_convention"] = build_identifiant_convention(df_remuneration)

df_convention["identifiant_convention"] = build_identifiant_convention(df_convention)

# ### Fonctions utilitaires pour analyse/exploration

# +
def get_company(identifiant):
    return df_company[df_company.identifiant_entreprise == identifiant]

def get_remunerations(identifiant_convention):
    return df_remuneration[df_remuneration.identifiant_convention == identifiant_convention]

def get_convention(identifiant_convention):
    return df_convention[df_convention.identifiant_convention == identifiant_convention]

def get_avantages(identifiant_convention):
    return df_avantage[df_avantage.identifiant_convention == identifiant_convention]

# -

identifiant_convention = "JUJJFUKK|MANUEL_297341"
get_convention(identifiant_convention)

print(get_remunerations(identifiant_convention).shape[0])
get_remunerations(identifiant_convention).head()

get_avantages(identifiant_convention)

# ### Analyse : nombre de remuneration/avantage avec le même identifiant

nb_id_rem = df_remuneration.identifiant_convention.value_counts()
nb_id_rem.value_counts().head()

nb_id_avan = df_avantage.identifiant_convention.value_counts()
nb_id_avan.value_counts().head()

# ### Jointure


# +
res_rem = df_remuneration.groupby('identifiant_convention')['montant_ttc'].agg([np.sum, np.size])
res_rem.columns = ['total_montant_remunerations_lies', 'nombre_remunerations_lies']

res_rem.shape

# +
res_avan = df_avantage.dropna(subset=['identifiant_convention']).groupby('identifiant_convention')['montant_ttc'].agg([np.sum, np.size])
res_avan.columns = ['total_montant_avantages_lies', 'nombre_avantages_lies']

res_avan.shape

# +
df_conv = df_convention.merge(
    res_rem,    
    how='left',
    left_on='identifiant_convention',
    right_index=True,
    validate='one_to_one'
).merge(
    res_avan, 
    how='left',
    left_on='identifiant_convention',
    right_index=True,
    validate='one_to_one'

)
df_conv[res_rem.columns] = df_conv[res_rem.columns].fillna(0)
df_conv[res_avan.columns] = df_conv[res_avan.columns].fillna(0)

# -

df_conv['total_montant_lies'] = df_conv['total_montant_avantages_lies'] + df_conv['total_montant_remunerations_lies']

columns = list(res_rem.columns) + list(res_avan.columns) + ['total_montant_lies', 'montant_masque', 'montant_ttc']
df_conv[columns].head()

# ### Analyse de la répartition des cas
# - aucun lie 3069816
# - lie 996989
#     - montant_ttc is na 774791
#     - montant_ttc < montant_lie  10069
#     - montant_ttc = montant_lie  183051
#     - montant_ttc > montant_lie 29078

mask_lie = df_conv.nombre_avantages_lies + df_conv.nombre_remunerations_lies > 0
mask_lie.value_counts()

mask_na = df_conv.montant_ttc.isna()
mask_na[mask_lie].value_counts()

mask_inf = df_conv.montant_ttc < df_conv['total_montant_lies']
mask_inf[mask_lie].value_counts()

mask_egal = df_conv.montant_ttc == df_conv['total_montant_lies']
mask_egal[mask_lie].value_counts()

mask_sup = df_conv.montant_ttc > df_conv['total_montant_lies']
mask_sup[mask_lie].value_counts()

774791 + 10069 + 183051 + 29078 == 996989

# ### Création colones montant pour les conventions
# - Création des colonnes
#   - montant_declare_convention : montant_ttc original # pour garder la trace
#   - montant_total_convention : max(montant_declare_convention.fillna(0), total_montant_lies) # info intéressante
#   - montant_ttc : montant_total_convention - montant_declare_convention.fillna(0) # rattrapage de ce qui n'est pas déclaré ailleurs
#   - montant_masque : montant_total_convention > 0 

df_conv['montant_declare_convention'] = df_conv.montant_ttc

df_conv['montant_total_convention'] = df_conv[['montant_declare_convention', 'total_montant_lies']].fillna(0).max(axis='columns')

df_conv['montant_ttc'] = df_conv['montant_total_convention'] - df_conv['total_montant_lies'].fillna(0)

df_conv['montant_masque'] = df_conv['montant_total_convention'] == 0

df_conv['montant_masque'].value_counts()


