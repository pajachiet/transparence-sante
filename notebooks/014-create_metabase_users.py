# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: transparence-sante
#     language: python
#     name: transparence-sante
# ---

# <h1 id="tocheading">Summary</h1>
# <div id="toc"></div>

# + {"language": "javascript"}
# $.getScript('https://kmahelona.github.io/ipython_notebook_goodies/ipython_notebook_toc.js')
# -

# # Setup

# ## Generic import and setup

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

import os
import sys

ROOT_PATH = os.path.dirname(os.getcwd())
os.chdir(ROOT_PATH)
sys.path.append(ROOT_PATH)

# +
import numpy as np
import pandas as pd

pd.options.display.max_rows = 30
pd.options.display.max_columns = 100
pd.options.display.max_colwidth = 100

import matplotlib.pyplot as plt
from matplotlib_venn import venn2, venn3
from pprint import pprint
# -

# ## Specific import

from settings import *
from src.metabase.api import *

import sys

if len(sys.argv) < 2:
    print("Usage : email [prenom] [nom]")
    exit(0)

email = sys.argv[1]
first_name_mail, last_name_mail = (email.split("@")[0].split(".") + ["nom"])[0:2]

if len(sys.argv) > 2:
    first_name = sys.argv[2]
else:
    first_name = first_name_mail

if len(sys.argv) > 3:
    last_name = sys.argv[3]
else:
    last_name = last_name_mail

print("email :", email, ", prénom :", first_name, ", nom :", last_name)

# # Use metabase API

print(METABASE_BASE_URL)

# ## Get token and generic elements

token = metabase_get_token()

metabase_get("/api/user/current", token)

payload = {
    "first_name": first_name,
    "last_name": last_name,
    "email": email
}
try:
    metabase_post("/api/user/", token, payload)
except ValueError as e:
    print(e)
    users = metabase_get("/api/user/", token, {"query": email})
    pprint(users['data'][0])


