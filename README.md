*This project aims to make it easy to use [Transparence Santé](www.transparence.sante.gouv.fr) dataset, 
as inspired by ProPublica's [DollarsForDocs](https://projects.propublica.org/docdollars) project.
It supports [EurosForDocs](http://www.eurosfordocs.fr/) website*
 
Ce projet a pour objectif de simplifier l'utilisation de la base [Transparence-Santé](www.transparence.sante.gouv.fr), 
de façon similaire au site [DollarsForDocs](https://projects.propublica.org/docdollars) créé par ProPublica.
Il est en ligne à l'adresse [www.EurosForDocs.fr](http://www.eurosfordocs.fr/).

# Installation

Installation should be straightforward on UNIX systems (Mac, Linux). It has not been tested on Windows.

## Clone project 

Execute the following commands in a terminal 

    git clone https://gitlab.com/eurosfordocs/transparence-sante.git
    cd transparence-sante
    cp .env-template .env  # Create a default .env file from template

## Python data processing 

For local development, we recommend to manage dependencies with `virtualenv`.

**Requirements**
- Python 3.6 or 3.7
    - test with `python3 --version`
- pip3
    - test with `pip3 --version`
- virtualenv
    - test with `virtualenv --version`
    - install with `pip3 install virtualenv`

**Installation** 

Create virtual environment and activate it.

    virtualenv venv --python=python3
    source venv/bin/activate

Install Python dependencies 

    pip3 install -r requirements.txt

**Test**

This should run >35 tests

    pytest --ignore=tests/docker

**Usage** 

If not already done, activate the virtualenv. 

    source venv/bin/activate
    
The entrypoint to the Python code is the script `main.py`. 
You can list the different script possibilities with `./main.py --help`. 

To download and clean the data, run the following commands:
```
    ./main.py download  # Download files to data/raw
    ./main.py extract   # Extract CSV files to data/raw
    ./main.py clean     # Clean CSV files to data/cleaned and data/joined
```

### Jupyter

Jupyter notebook are a great way to interactively explore and analyze data.

To collaborate on Jupyter Notebooks, we use [JupyTEXT](https://github.com/mwouts/jupytext) plugin. 

JupyTEXT allows to read and write Jupyter notebooks as paired `.py` files.

**Usage**

    cd notebooks
    ./jupyter.sh

### Integrated Development Environment

We recommend to use an IDE such as PyCharm to navigate and modify the codebase.

**PyCharm Tips**
- PyCharm professional version comes with helpful Docker's and Databases' interfaces. 
- JetBrains supports EurosForDocs with [Free Open Source Licences](https://www.jetbrains.com/buy/opensource/)
- Exclude the following directories from indexing `(right click > Mark directory as)` : 
venv, data, docker/SERVICE/data, notebooks/.ipynb_checkpoints

## Running the whole project with Docker-Compose

This project uses **Docker** to run PostgreSQL, Metabase, and others services.

You can also run Python data processing in a Docker image.   

![architecture](images/architecture.png)

**Requirements**
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker-Compose](https://docs.docker.com/compose/install/)

**Installation**

*Note: this will download heavy Docker images, and may take some times to build them.* 

    docker-compose up           # <Ctrl> + C to stop
 
**Test**

When all services are up

- [localhost](http://localhost) serve website's homepage
- Metabase is running at [localhost/metabase/](http://localhost/metabase/)
- Run the tests ```docker-compose build python; docker-compose run --rm python pytest --ignore tests/docker/metabase``` 

**Usage**

See Docker-Compose documentation. 

Run `./docker_compose_helper.py` to see some common commands.


# Contributing

Contributions are most welcome !

Please look at 
- [Gitlab issues](https://gitlab.com/eurosfordocs/transparence-sante/issues)
- [Contributing page](http://www.eurosfordocs.fr/contributing/) on website
